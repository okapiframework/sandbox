package net.sf.okapi.resourcesimplifier.integration;


import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.ZipFileCompare;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.skeleton.GenericSkeletonWriter;
import net.sf.okapi.filters.idml.IDMLFilter;

import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripIdmlWithSimplifierIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	private IDMLFilter idmlFilter;
	private String[] testFileList;
	private String root;

	@Before
	public void setUp() throws Exception {
		idmlFilter = new IDMLFilter();
		testFileList = getIdmlFiles();
		URL url = RoundTripIdmlWithSimplifierIT.class.getResource("/idml/idmltest.idml");
		root = Util.getDirectoryName(url.toURI().getPath()) + File.separator;
	}

	@After
	public void tearDown() throws Exception {
		idmlFilter.close();
	}

	//@Test
	// XXX The test is replaced with 
	// net.sf.okapi.resourcesimplifier.integration.doublex.DoubleExtractionWithSimplifier#testIdmlFilter()
	public void roundTripIdmlFiles() throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);		
			String original = root + f;
			String simplified = root+f+".simp";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(idmlFilter, rd, null);
			RoundTripUtils.writeSimplified(false, events, (GenericSkeletonWriter) idmlFilter.createSkeletonWriter(), simplified, rd.getEncoding());
			ZipFileCompare compare = new ZipFileCompare(); 
			assertTrue(compare.compareFilesPerLines(original, simplified, rd.getEncoding()));
		}
	}

	private static String[] getIdmlFiles() throws URISyntaxException {
		// read all files in the test xml directory
		URL url = RoundTripIdmlWithSimplifierIT.class.getResource("/idml/idmltest.idml");
		File dir = new File(url.toURI()).getParentFile();

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".idml");
			}
		};
		return dir.list(filter);
	}
}
