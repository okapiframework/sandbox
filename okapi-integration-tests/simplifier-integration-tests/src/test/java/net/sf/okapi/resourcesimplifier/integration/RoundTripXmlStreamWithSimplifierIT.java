package net.sf.okapi.resourcesimplifier.integration;


import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.xmlstream.XmlStreamFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripXmlStreamWithSimplifierIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	private XmlStreamFilter xmlStreamFilter;
	private String[] testFileList;

	@Before
	public void setUp() throws Exception {
		xmlStreamFilter = new XmlStreamFilter();
		testFileList = getXmlFles(".xml");
	}

	@After
	public void tearDown() throws Exception {
		xmlStreamFilter.close();
	}

	@Test
	public void roundTripXmlStream() throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);		
			String original = f;
			String simplified = f+".simp";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(xmlStreamFilter, rd, null);
			RoundTripUtils.writeSimplified(events, simplified, rd.getEncoding());
			// must compare events as xml is normalized on output
			RawDocument ord = new RawDocument(Util.toURI(original), rd.getEncoding(), LocaleId.ENGLISH);
			RawDocument trd = new RawDocument(Util.toURI(simplified), rd.getEncoding(), LocaleId.ENGLISH);
			assertTrue(FilterTestDriver.compareEvents(FilterTestDriver.getEvents(xmlStreamFilter, ord, null), 
					FilterTestDriver.getEvents(xmlStreamFilter, trd, null), true));		
		}
	}
	
	@Test
	public void roundTripXmlStreamDita() throws FileNotFoundException, URISyntaxException {
		xmlStreamFilter.setParametersFromURL(RoundTripXmlStreamWithSimplifierIT.class
				.getResource("/net/sf/okapi/filters/xmlstream/dita.yml"));
		testFileList = getXmlFles(".dita");
		for (String f : testFileList) {
			LOGGER.trace(f);		
			String original = f;
			String gold = f + ".gold";
			String simplified = f+".simp";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(xmlStreamFilter, rd, null);
			
			RoundTripUtils.writeOut(events, gold, rd.getEncoding());
			RoundTripUtils.writeSimplified(events, simplified, rd.getEncoding());
			
			FileCompare compare = new FileCompare();
			assertTrue(compare.compareFilesPerLines(simplified, gold, rd.getEncoding()));
		}
	}
	
	@Test
	public void roundTripXmlStreamJavaProp() throws FileNotFoundException, URISyntaxException {
		xmlStreamFilter.setParametersFromURL(RoundTripXmlStreamWithSimplifierIT.class
				.getResource("/net/sf/okapi/filters/xmlstream/javaPropertiesXml.yml"));
		testFileList = new String[] {"JavaProperties.xml"};
		String root = RoundTripUtils.getRoot(this.getClass(), "/xml/input.xml", testFileList);
		for (String f : testFileList) {
			LOGGER.trace(f);		
			String original = root + f;
			String simplified = root + f + ".simp";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(xmlStreamFilter, rd, null);
			RoundTripUtils.writeSimplified(events, simplified, rd.getEncoding());
			// must compare events as xml is normalized on output
			RawDocument ord = new RawDocument(Util.toURI(original), rd.getEncoding(), LocaleId.ENGLISH);
			RawDocument trd = new RawDocument(Util.toURI(simplified), rd.getEncoding(), LocaleId.ENGLISH);
			assertTrue(FilterTestDriver.compareEvents(FilterTestDriver.getEvents(xmlStreamFilter, ord, null), 
					FilterTestDriver.getEvents(xmlStreamFilter, trd, null), true));		
		}
	}

	private static String[] getXmlFles(final String ext) throws URISyntaxException {
		return RoundTripUtils.getTestFiles(RoundTripXmlStreamWithSimplifierIT.class, "/xml/input.xml",
				ext);
		
//		// read all files in the test xml directory
//		URL url = RoundTripXmlStreamWithSimplifierIT.class
//				.getResource("/xml/input.xml");
//		File dir = new File(url.toURI()).getParentFile();
//
//		FilenameFilter filter = new FilenameFilter() {
//			@Override
//			public boolean accept(File dir, String name) {
//				return name.endsWith(ext);
//			}
//		};
//		return dir.list(filter);
	}
}
