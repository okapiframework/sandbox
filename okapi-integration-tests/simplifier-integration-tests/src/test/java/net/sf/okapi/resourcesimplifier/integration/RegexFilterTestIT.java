/*===========================================================================
  Copyright (C) 2008-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.resourcesimplifier.integration;

import static net.sf.okapi.resourcesimplifier.integration.RoundTripUtils.simplifyEvents;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.regex.Parameters;
import net.sf.okapi.filters.regex.RegexFilter;
import net.sf.okapi.filters.regex.Rule;

import org.junit.Before;
import org.junit.Test;

public class RegexFilterTestIT
{
	
	private RegexFilter filter;
	private final LocaleId locEN = LocaleId.fromString("en");
	
	@Before
	public void setUp() {
		filter = new RegexFilter();
//		RoundTripUtils.path = TestUtil.getParentDir(this.getClass(), "/dummy.txt")+"test_regex.json";
	}
	
	@Test
	public void testSimpleRule () {
		String snippet = "test1=\"text1\"\ntest2=\"text2\"\n";
		Parameters params = new Parameters();
		Rule rule = new Rule();
		rule.setRuleType(Rule.RULETYPE_STRING);
		rule.setExpression("=(.+)$");
		rule.setSourceGroup(1);
		params.getRules().add(rule);
		filter.setParameters(params);
		// Process
		List<Event> list = simplifyEvents(getEvents(snippet));
		ITextUnit tu = FilterTestDriver.getTextUnit(list, 1);
		assertNotNull(tu);
		assertEquals("text1", tu.getSource().toString());
		tu = FilterTestDriver.getTextUnit(list, 2);
		assertNotNull(tu);
		assertEquals("text2", tu.getSource().toString());
	}

	@Test
	public void testIDAndText () {
		String snippet = "[Id1]\tText1\r\n[Id2]\tText2";
		Parameters params = new Parameters();
		Rule rule = new Rule();
		rule.setRuleType(Rule.RULETYPE_CONTENT);
		rule.setExpression("^\\[(.*?)]\\s*(.*?)(\\n|\\Z)");
		rule.setSourceGroup(2);
		rule.setNameGroup(1);
		rule.setPreserveWS(true);
		params.getRules().add(rule);
		filter.setParameters(params);
		// Process
		List<Event> list = simplifyEvents(getEvents(snippet));
		ITextUnit tu = FilterTestDriver.getTextUnit(list, 1);
		assertNotNull(tu);
		assertEquals("Text1", tu.getSource().toString());
		assertEquals("Id1", tu.getName());
		tu = FilterTestDriver.getTextUnit(list, 2);
		assertNotNull(tu);
		assertEquals("Text2", tu.getSource().toString());
		assertEquals("Id2", tu.getName());
	}

	@Test
	public void testEscapeDoubleChar () {
		String snippet = "id = [\"\"\"a\"\"b\"\"c\"\"\"]";
		Parameters params = new Parameters();
		Rule rule = new Rule();
		rule.setRuleType(Rule.RULETYPE_CONTENT);
		rule.setExpression("^.*?\\[(.*?)]");
		rule.setSourceGroup(1);
		rule.setRuleType(Rule.RULETYPE_STRING);
		params.getRules().add(rule);
		params.setUseDoubleCharEscape(true);
		filter.setParameters(params);
		// Process
		List<Event> list = simplifyEvents(getEvents(snippet));
		ITextUnit tu = FilterTestDriver.getTextUnit(list, 1);
		assertNotNull(tu);
		assertEquals("\"\"a\"\"b\"\"c\"\"", tu.getSource().toString());
	}

	@Test
	public void testEscapeDoubleCharNoEscape () {
		String snippet = "id = [\"a\" and \"b\"]";
		Parameters params = new Parameters();
		Rule rule = new Rule();
		rule.setRuleType(Rule.RULETYPE_CONTENT);
		rule.setExpression("^.*?\\[(.*?)]");
		rule.setSourceGroup(1);
		rule.setRuleType(Rule.RULETYPE_STRING);
		params.getRules().add(rule);
		params.setUseDoubleCharEscape(true);
		filter.setParameters(params);
		// Process
		List<Event> list = simplifyEvents(getEvents(snippet));
		ITextUnit tu = FilterTestDriver.getTextUnit(list, 1);
		assertNotNull(tu);
		assertEquals("a", tu.getSource().toString());
		tu = FilterTestDriver.getTextUnit(list, 2);
		assertNotNull(tu);
		assertEquals("b", tu.getSource().toString());
	}

	private List<Event> getEvents(String snippet) {
		ArrayList<Event> list = new ArrayList<Event>();
		filter.open(new RawDocument(snippet, locEN));
		while (filter.hasNext()) {
			Event event = filter.next();
			list.add(event);
		}
		filter.close();
		return list;
	}	
}

