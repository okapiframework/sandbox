package net.sf.okapi.resourcesimplifier.integration;


import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.regex.RegexFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripRegexWithSimplifierIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	private  RegexFilter regexFilter;
	private String[] testFileList;

	@Before
	public void setUp() throws Exception {
		regexFilter = new RegexFilter();
		testFileList = getRegexFiles();
	}

	@After
	public void tearDown() throws Exception {
		regexFilter.close();
	}

	@Test
	public void roundTripRegexFiles() throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);		
			String original = f;
			String simplified = f+".simp";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH, LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(regexFilter, rd, null);
			RoundTripUtils.writeSimplified(events, simplified, rd.getEncoding());
			
			FileCompare compare = new FileCompare();
			assertTrue(compare.compareFilesPerLines(original, simplified, rd.getEncoding()));
		}
	}

	private static String[] getRegexFiles() throws URISyntaxException {
		return RoundTripUtils.getTestFiles(RoundTripRegexWithSimplifierIT.class, "/regex/Test01_stringinfo_en.info",
				".info");
		
//		URL url = RoundTripRegexWithSimplifierIT.class.getResource("/regex/Test01_stringinfo_en.info");
//		File dir = new File(url.toURI()).getParentFile();
//
//		FilenameFilter filter = new FilenameFilter() {
//			@Override
//			public boolean accept(File dir, String name) {
//				return name.endsWith(".info");
//			}
//		};
//		return dir.list(filter);
	}
}
