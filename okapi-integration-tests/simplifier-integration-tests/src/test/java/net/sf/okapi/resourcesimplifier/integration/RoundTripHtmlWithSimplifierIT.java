package net.sf.okapi.resourcesimplifier.integration;


import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.ListUtil;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.lib.extra.pipelinebuilder.XBatch;
import net.sf.okapi.lib.extra.pipelinebuilder.XBatchItem;
import net.sf.okapi.lib.extra.pipelinebuilder.XPipeline;
import net.sf.okapi.lib.extra.steps.TuDpLogger;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;
import net.sf.okapi.steps.common.ResourceSimplifierStep;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripHtmlWithSimplifierIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	private HtmlFilter htmlFilter;
	private String[] testFileList;

	@Before
	public void setUp() throws Exception {
		htmlFilter = new HtmlFilter();		
		htmlFilter.setParametersFromURL(this.getClass().getResource("nonwellformedConfiguration.yml"));
		testFileList = getHtmlTestFiles();
	}

	@After
	public void tearDown() throws Exception {
		htmlFilter.close();
	}

	@Test
	public void roundTripHtmlNonWellformed() throws FileNotFoundException {
		htmlFilter.setParametersFromURL(this.getClass().getResource("nonwellformedConfiguration.yml"));
		for (String f : testFileList) {
			LOGGER.trace(f);		
			String original = f;
			String simplified = f+".simp";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(htmlFilter, rd, null);
			RoundTripUtils.writeSimplified(events, simplified, rd.getEncoding());
			
			// must compare events as html is normalized on output
			RawDocument ord = new RawDocument(Util.toURI(original), rd.getEncoding(), LocaleId.ENGLISH);
			RawDocument trd = new RawDocument(Util.toURI(simplified), rd.getEncoding(), LocaleId.ENGLISH);
			assertTrue(FilterTestDriver.compareEvents(
					FilterTestDriver.getEvents(htmlFilter, trd, null), // out
					FilterTestDriver.getEvents(htmlFilter, ord, null), // gold
					true));		
			}
	}
	
	private boolean isExcluded(String f, List<String> exFiles) {
		for (String exFile : exFiles) {
			if (f.endsWith(exFile)) return true;
		}
		return false;
	}
	
	@Test
	public void roundTripHtmlWellformed() throws FileNotFoundException {
		htmlFilter.setParametersFromURL(this.getClass().getResource("wellformedConfiguration.yml"));
		List<String> exFiles = ListUtil.arrayAsList(new String[] {
				// These files are non-wellformed
				"324.html",
				"ugly_big.htm"
			});
		
		for (String f : testFileList) {
			if (isExcluded(f, exFiles)) continue;
			
			LOGGER.trace(f);	
			
			String original = f;
			String gold = f+".gold";
			String simplified = f+".simp";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(htmlFilter, rd, null);
			RoundTripUtils.writeOut(events, gold, LocaleId.FRENCH, rd.getEncoding());
			RoundTripUtils.writeSimplified(false, events, null, simplified, LocaleId.FRENCH, rd.getEncoding());			
		
			FileCompare compare = new FileCompare();
			assertTrue(compare.compareFilesPerLines(simplified, gold, rd.getEncoding(), false, true));
		}
	}
	
	@Test
	public void roundTripHtmlWellformed2() throws FileNotFoundException {
		htmlFilter.setParametersFromURL(this.getClass().getResource("wellformedConfiguration.yml"));
		List<String> exFiles = ListUtil.arrayAsList(new String[] {
			// These files are non-wellformed
			"324.html",
			"ugly_big.htm"
		});
		
		for (String f : testFileList) {
			if (isExcluded(f, exFiles)) continue;
			
			LOGGER.trace(f);
			
			String original = f;
			String gold = f+".gold";
			String simplified = f+".simp";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(htmlFilter, rd, null);
			RoundTripUtils.writeOut(events, gold, rd.getEncoding());
			RoundTripUtils.writeSimplified(events, simplified, rd.getEncoding());			
		
			FileCompare compare = new FileCompare();
			assertTrue(compare.compareFilesPerLines(simplified, gold, rd.getEncoding(), false, true));
		}
	}
	
	@Test
	public void testSpan() {
		String snippet = "<p>Text before span <span>Span text</span> Text after span</p>";				
		listSnippetEvents(snippet);
	}
	
	@Test
	public void testSimplifiedSpan() {
		String snippet = "<p>Text before span <span>Span text</span> Text after span</p>";				
		listSimplifiedSnippetEvents(snippet);
	}
	
	@Test
	public void testSimplifiedSpan2() {
		htmlFilter.setParametersFromURL(this.getClass().getResource("wellformedConfiguration.yml"));
		String snippet = "<span id=\"tgt37\" sentenceId=\"fcca9ba142c9b3d03d2c76cf5303a12a\" class=\"tgtSentence\">An implementation of the <b>T:System.Windows.Input.ICommand</b> interface is frequently used with the MVVM pattern.</span>";				
		listSimplifiedSnippetEvents(snippet);
	}
	
//	@Test
//	public void testSimplifiedEvents() throws URISyntaxException {
//		listSimplifiedEvents(root + "ugly_big.htm");
//	}
//	
//	@Test
//	public void testEvents() throws URISyntaxException {
////		listEvents(root + "ugly_big.htm");
//		listEvents(root + "ugly_big.htm.simp");
//	}
	
	private void listSnippetEvents(String snippet) {
		new XPipeline(
				null,
				new XBatch(
						new XBatchItem(
								new RawDocument(snippet, LocaleId.ENGLISH)								
						)
				),
						
				new RawDocumentToFilterEventsStep(htmlFilter),
				new TuDpLogger()
		).execute();
	}
	
	private void listSimplifiedSnippetEvents(String snippet) {
		new XPipeline(
				null,
				new XBatch(
						new XBatchItem(
								new RawDocument(snippet, LocaleId.ENGLISH)								
						)
				),
						
				new RawDocumentToFilterEventsStep(htmlFilter),
				new ResourceSimplifierStep(),
				new TuDpLogger()
		).execute();
	}
	
	private void listEvents(String fname) throws URISyntaxException {
		IFilter filter = new HtmlFilter();
		
		new XPipeline(
				null,
				new XBatch(
						new XBatchItem(
								Util.toURI(fname),
								"UTF-8",
								LocaleId.ENGLISH)
						),
						
				new RawDocumentToFilterEventsStep(filter),
				new TuDpLogger()
		).execute();
	}
	
	private void listSimplifiedEvents(String fname) throws URISyntaxException {
		IFilter filter = new HtmlFilter();
		
		new XPipeline(
				null,
				new XBatch(
						new XBatchItem(
								Util.toURI(fname),
								"UTF-8",
								LocaleId.ENGLISH)
						),
						
				new RawDocumentToFilterEventsStep(filter),
				new ResourceSimplifierStep(),
				new TuDpLogger()
		).execute();
	}

	private static String[] getHtmlTestFiles() throws URISyntaxException {
		return RoundTripUtils.getTestFiles(RoundTripHtmlWithSimplifierIT.class, "/html/324.html",
				".html", ".htm", ".phtml");
//		// read all files in the test html directory
//		URL url = RoundTripHtmlWithSimplifierIT.class.getResource("/html/324.html");
//		File dir = new File(url.toURI()).getParentFile();
//
//		FilenameFilter filter = new FilenameFilter() {
//			@Override
//			public boolean accept(File dir, String name) {
//				return name.endsWith(".html") || name.endsWith(".htm")
//						|| name.endsWith(".phtml");
//			}
//		};
//		return dir.list(filter);
	}
}
