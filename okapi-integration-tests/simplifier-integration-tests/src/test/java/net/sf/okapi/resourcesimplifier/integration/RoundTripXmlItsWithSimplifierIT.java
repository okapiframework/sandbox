package net.sf.okapi.resourcesimplifier.integration;


import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.skeleton.GenericSkeletonWriter;
import net.sf.okapi.filters.its.html5.HTML5Filter;
import net.sf.okapi.filters.xml.XMLFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripXmlItsWithSimplifierIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	private XMLFilter xmlFilter;
	private String[] testFileList;

	@Before
	public void setUp() throws Exception {
		xmlFilter = new XMLFilter();
		testFileList = getXmlItsFles();
	}

	@After
	public void tearDown() throws Exception {
		xmlFilter.close();
	}

	@Test
	public void roundTripXmlItsStream() throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);		
			String original = f;
			String simplified = f+".simp";
			String gold = f+".gold";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(xmlFilter, rd, null);
			RoundTripUtils.writeOut(events, gold, rd.getEncoding());
			RoundTripUtils.writeSimplified(true, events, (GenericSkeletonWriter) xmlFilter.createSkeletonWriter(), simplified, rd.getEncoding());
			
			FileCompare compare = new FileCompare();
			assertTrue(compare.compareFilesPerLines(simplified, gold, rd.getEncoding(), true, false));
		}
	}
	
	@Test
	public void roundTripHtmlItsStream() throws FileNotFoundException {
		HTML5Filter htmlFilter = new HTML5Filter();		
		testFileList = new String[] {"lqi-test1-standoff.html", "test01.html", "lqi-test1.html"};
		String root = RoundTripUtils.getRoot(this.getClass(), "/its/input.xml", testFileList);
		for (String f : testFileList) {
			LOGGER.trace(f);		
			String original = root + f;
			String simplified = root + f + ".simp";
			String gold = root + f + ".gold";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(htmlFilter, rd, null);
			RoundTripUtils.writeOut(events, gold, rd.getEncoding());
			RoundTripUtils.writeSimplified(false, events, (GenericSkeletonWriter) htmlFilter.createSkeletonWriter(), simplified, rd.getEncoding());

			FileCompare compare = new FileCompare();
			assertTrue(compare.compareFilesPerLines(simplified, gold, rd.getEncoding(), true, false));
		}
	}

	private static String[] getXmlItsFles() throws URISyntaxException {
		return RoundTripUtils.getTestFiles(RoundTripXmlItsWithSimplifierIT.class, "/its/input.xml",
				".xml");
		
//		// read all files in the test xml directory
//		URL url = RoundTripXmlItsWithSimplifierIT.class
//				.getResource("/its/input.xml");
//		File dir = new File(url.toURI()).getParentFile();
//
//		FilenameFilter filter = new FilenameFilter() {
//			@Override
//			public boolean accept(File dir, String name) {
//				return name.endsWith(".xml");
//			}
//		};
//		return dir.list(filter);
	}
}
