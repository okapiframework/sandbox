package net.sf.okapi.resourcesimplifier.integration.doublex;

import java.io.InputStream;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.lib.extra.pipelinebuilder.XBatch;
import net.sf.okapi.lib.extra.pipelinebuilder.XBatchItem;
import net.sf.okapi.lib.extra.pipelinebuilder.XParameter;
import net.sf.okapi.lib.extra.pipelinebuilder.XPipeline;
import net.sf.okapi.lib.extra.pipelinebuilder.XPipelineStep;
import net.sf.okapi.steps.common.FilterEventsToRawDocumentStep;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;
import net.sf.okapi.steps.common.ResourceSimplifierStep;
import net.sf.okapi.steps.common.copysource.CopySourceOnEmptyTargetStep;
import net.sf.okapi.steps.xliffkit.reader.XLIFFKitReaderStep;
import net.sf.okapi.steps.xliffkit.writer.XLIFFKitWriterStep;
import net.sf.okapi.tkit.integration.common.DoubleExtractionIT;

import org.slf4j.LoggerFactory;

public class DoubleExtractionWithSimplificationBeforeSerializationIT
		extends DoubleExtractionIT
{

	@Override
	protected void createOutputFile(boolean useExtraStep, IFilter filter, InputStream inStream, String outPath, String encoding, LocaleId srcLoc, LocaleId trgLoc) {
		if (!useExtraStep) {
//			super.createOutputFile(useExtraStep, filter, inPath, outPath, encoding, srcLoc,
//					trgLoc);
			new XPipeline(
					null,
					new XBatch(
							new XBatchItem(
									inStream,
									encoding,
									outPath,
									encoding,
									srcLoc,
									trgLoc)
							),
							
					new RawDocumentToFilterEventsStep(filter),
//					new XPipelineStep(
//							new CreateTargetStep(),
//							new XParameter("overwriteExisting", true)
//						),					
					new CopySourceOnEmptyTargetStep(),
					new FilterEventsToRawDocumentStep()
			).execute();
		}
		else {
			String tkitPath = outPath + String.format("_%s.%s.xliff.kit", srcLoc, trgLoc);
			LoggerFactory.getLogger(getClass()).info("T-kit path:\n" + Util.getDirectoryName(tkitPath));
			
			// Serialize events to XLIFF kit
			new XPipeline(
					null,
					new XBatch(
							new XBatchItem(
									inStream,
									encoding,
									tkitPath,
									encoding,
									srcLoc,
									trgLoc)
							),
							
					new RawDocumentToFilterEventsStep(filter),
//					new TuDpLogger(),
					new ResourceSimplifierStep(),
					new XPipelineStep(
							new XLIFFKitWriterStep(),
							new XParameter("copySource", true),
							// The following 2 params are needed off if we work with an input stream, not URI
							new XParameter("includeSource", false),
							new XParameter("includeOriginal", false)
						)
			).execute();
			
			// Deserialize events from XLIFF kit, create output file
			new XPipeline(
					null,
					new XBatch(
							new XBatchItem(
									Util.toURI(tkitPath),
									encoding,
									Util.toURI(outPath),
									encoding,
									srcLoc,
									trgLoc)
							),
							
					new XPipelineStep(
							new XLIFFKitReaderStep(),
							new XParameter("generateTargets", false),
							new XParameter("updateApprovedFlag", false))
					,			
//					new TuDpLogger(true),
					new FilterEventsToRawDocumentStep()
			).execute();
		}
	}
}
