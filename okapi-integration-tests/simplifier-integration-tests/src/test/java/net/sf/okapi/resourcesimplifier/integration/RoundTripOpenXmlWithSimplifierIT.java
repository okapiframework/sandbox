package net.sf.okapi.resourcesimplifier.integration;


import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.ZipFileCompare;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.openxml.OpenXMLContentSkeletonWriter;
import net.sf.okapi.filters.openxml.OpenXMLFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripOpenXmlWithSimplifierIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	private OpenXMLFilter openXmlFilter;
	private String[] testFileList;

	@Before
	public void setUp() throws Exception {
		openXmlFilter = new OpenXMLFilter();
		testFileList = getOpenXmlFiles();
	}

	@After
	public void tearDown() throws Exception {
		openXmlFilter.close();
	}

	@Test
//	@Ignore
	// FIXME: this test fails after Dan's latest openXml fixes
	public void roundTripOpenXml() throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);		
			String original = f;
			String gold = f + ".gold";
			String simplified = f+".simp";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(openXmlFilter, rd, null);
			RoundTripUtils.writeOut(events, gold, rd.getEncoding());
			RoundTripUtils.writeSimplified(false, events, new OpenXMLContentSkeletonWriter(1), simplified, rd.getEncoding());
			ZipFileCompare compare = new ZipFileCompare(); 
//			assertTrue(compare.compareFilesPerLines(simplified, gold, rd.getEncoding()));
//			assertTrue(compare.compareFilesPerLinesIgnoreCase(original, gold, rd.getEncoding(), true));
			assertTrue(compare.compareFilesPerLinesIgnoreCase(simplified, gold, rd.getEncoding(), true));
		}
	}
	
	@Test
	public void roundTripOpenXml2() throws FileNotFoundException {		
		String f = "TestLTinsideBoxFails.docx";
		String[] testFileList = new String[] {f};
		String root = RoundTripUtils.getRoot(this.getClass(), "/openxml/Deli.docx", testFileList);
		LOGGER.trace(f);		
		String original = root + f;
		String gold = root + f + ".gold";
		String simplified = root + f + ".simp";
		
		RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
		List<Event> events = FilterTestDriver.getEvents(openXmlFilter, rd, null);
		RoundTripUtils.writeOut(events, gold, rd.getEncoding());
		RoundTripUtils.writeSimplified(false, events, new OpenXMLContentSkeletonWriter(1), simplified, rd.getEncoding());
		ZipFileCompare compare = new ZipFileCompare(); 
//			assertTrue(compare.compareFilesPerLines(simplified, gold, rd.getEncoding()));
//			assertTrue(compare.compareFilesPerLinesIgnoreCase(original, gold, rd.getEncoding(), true));
		assertTrue(compare.compareFilesPerLinesIgnoreCase(simplified, gold, rd.getEncoding(), true));
	}

	private static String[] getOpenXmlFiles() throws URISyntaxException {
		return RoundTripUtils.getTestFiles(RoundTripOpenXmlWithSimplifierIT.class, "/openxml/Deli.docx",
				".docx", ".xlsx");
		
//		// read all files in the test xml directory
//		URL url = RoundTripOpenXmlWithSimplifierIT.class.getResource("/openxml/Deli.docx");
//		File dir = new File(url.toURI()).getParentFile();
//
//		FilenameFilter filter = new FilenameFilter() {
//			@Override
//			public boolean accept(File dir, String name) {
//				return name.endsWith(".docx") || name.endsWith(".xlsx");
//			}
//		};
//		return dir.list(filter);
	}
}
