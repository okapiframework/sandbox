package net.sf.okapi.resourcesimplifier.integration.doublex;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Stack;

import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.common.resource.simplifier.IResourceSimplifier;
import net.sf.okapi.common.skeleton.GenericSkeletonSimplifier;
import net.sf.okapi.steps.common.ResourceSimplifierStep;
import net.sf.okapi.tkit.integration.common.DoubleExtractionIT;

import org.junit.Test;

public class DoubleExtractionWithSimplifierIT
		extends DoubleExtractionIT
{

	@Override
	protected IPipelineStep[] getPipelineSteps() {
		return new IPipelineStep[] {
//				new EventLogger(),
				new ResourceSimplifierStep()//,
//				new TuDpSgLogger()				
				};
	}
	
	@Test
	public void testPushNull() {
		Stack<IResourceSimplifier> stack = new Stack<IResourceSimplifier>();
		assertEquals(0, stack.size());
		
		stack.push(new GenericSkeletonSimplifier(locEN));
		assertEquals(1, stack.size());
		
		stack.push(null);
		assertEquals(2, stack.size());
		
		stack.push(new GenericSkeletonSimplifier(locFR));
		assertEquals(3, stack.size());
		
		stack.push(null);
		assertEquals(4, stack.size());
		
		stack.push(null);
		assertEquals(5, stack.size());
		
		IResourceSimplifier simplifier = stack.pop();
		assertEquals(4, stack.size());
		assertNull(simplifier);
		
		simplifier = stack.pop();
		assertEquals(3, stack.size());
		assertNull(simplifier);
		
		simplifier = stack.pop();
		assertEquals(2, stack.size());
		assertNotNull(simplifier);
		
		simplifier = stack.pop();
		assertEquals(1, stack.size());
		assertNull(simplifier);
		
		simplifier = stack.pop();
		assertEquals(0, stack.size());
		assertNotNull(simplifier);
	}
	// XXX !!! All tests should be placed as public in DoubleExtraction, this way they can be run
	// from both this DoubleExtractionWithSimplifier class and from DoubleExtraction.
	// Results will be different because ResourceSimplifierStep will be present only in DoubleExtractionWithSimplifier.
	
//	@Test
//	public void lisEvents_OpenXmlFilter2 () throws InstantiationException, IllegalAccessException, URISyntaxException {
//		IFilter filter = new OpenXMLFilter();
//		
//		listEvents(loadFilter(filter, "wordConfiguration.yml"), OpenXMLTest.class, "UTF-8", locEN, locFR,			
//			"EndGroup.docx");
//	}
//	
//	@Test
//	public void listEvents_POFilter () throws URISyntaxException {
//		IFilter filter = new POFilter();
//        
//        listEvents(filter, POFilterTest.class, "UTF-8", locEN, locFR, "AllCasesTest.po");
//	}
//	
//	@Test
//	public void listEvents_POFilter3 () throws URISyntaxException {
//		IFilter filter = new POFilter();
//        
//        listEvents(filter, POFilterTest.class, "UTF-8", locEN, locFR, "Test_nautilus.af.po");
//	}
//	
//	@Test
//	public void listEvents_HtmlFilter () throws URISyntaxException {
//		IFilter filter = new HtmlFilter();
//        
//        listEvents(filter, HtmlSnippetsTest.class, "UTF-8", locEN, locFR, "burlington_ufo_center.html");
//	}
//	
//	@Test
//	public void listEvents_XliffFilter () throws URISyntaxException {
//		IFilter filter = new XLIFFFilter();
//        
//        listEvents(filter, XLIFFFilterTest.class, "UTF-8", locEN, locFR, "BinUnitTest01.xlf");
//	}
//	
//	@Test
//	public void listEvents_XmlStreamFilter () throws URISyntaxException {
//		IFilter xmlStreamFilter = new XmlStreamFilter();
//		FilterConfigurationMapper fcMapper = new FilterConfigurationMapper();
//		fcMapper.addConfigurations(net.sf.okapi.filters.html.HtmlFilter.class.getName());
//		fcMapper.addConfigurations(net.sf.okapi.filters.xmlstream.XmlStreamFilter.class.getName());
//		fcMapper.setCustomConfigurationsDirectory(ClassUtil.getResourceParent(CdataSubfilterWithRegexTest.class, "/bookmap-readme.dita"));
//        fcMapper.addCustomConfiguration("okf_html@spaces_freemarker_regex");
//        fcMapper.addCustomConfiguration("okf_html@spaces_freemarker_no_regex");
//        fcMapper.updateCustomConfigurations();
//        xmlStreamFilter.setFilterConfigurationMapper(fcMapper);
//        
//        listEvents(loadFilter(xmlStreamFilter, "dita.yml"), PIExtractionTest.class, 
//				"UTF-8", locEN, locFR, "PI-Problem.xml");
//	}
}
