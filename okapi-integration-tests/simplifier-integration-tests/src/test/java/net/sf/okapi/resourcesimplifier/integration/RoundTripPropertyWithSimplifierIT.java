package net.sf.okapi.resourcesimplifier.integration;


import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.properties.PropertiesFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripPropertyWithSimplifierIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	private PropertiesFilter propertyFilter;
	private String[] testFileList;

	@Before
	public void setUp() throws Exception {
		propertyFilter = new PropertiesFilter();
		testFileList = getPropertiesFiles();
	}

	@After
	public void tearDown() throws Exception {
		propertyFilter.close();
	}

	@Test
	public void roundTripPropertiesFiles() throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);		
			String original = f;
			String simplified = f+".simp";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(propertyFilter, rd, null);
			RoundTripUtils.writeSimplified(events, simplified, rd.getEncoding());
			
			// must compare events as properies are normalized on output
			RawDocument ord = new RawDocument(Util.toURI(original), rd.getEncoding(), LocaleId.ENGLISH);
			RawDocument trd = new RawDocument(Util.toURI(simplified), rd.getEncoding(), LocaleId.ENGLISH);
			assertTrue(FilterTestDriver.compareEvents(FilterTestDriver.getEvents(propertyFilter, ord, null), 
					FilterTestDriver.getEvents(propertyFilter, trd, null), true));
		}
	}

	private static String[] getPropertiesFiles() throws URISyntaxException {
		return RoundTripUtils.getTestFiles(RoundTripPropertyWithSimplifierIT.class, "/property/Test01.properties",
				".properties");
		
//		URL url = RoundTripPropertyWithSimplifierIT.class.getResource("/property/Test01.properties");
//		File dir = new File(url.toURI()).getParentFile();
//
//		FilenameFilter filter = new FilenameFilter() {
//			@Override
//			public boolean accept(File dir, String name) {
//				return name.endsWith(".properties");
//			}
//		};
//		return dir.list(filter);
	}
}
