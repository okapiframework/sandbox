package net.sf.okapi.resourcesimplifier.integration;


import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.po.POFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripPoWithSimplifierIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	private POFilter poFilter;
	private String[] testFileList;

	@Before
	public void setUp() throws Exception {
		poFilter = new POFilter();
		testFileList = getPoFiles();
	}

	@After
	public void tearDown() throws Exception {
		poFilter.close();
	}

	@Test
	public void roundTripPoFiles() throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);		
			String original = f;
			String gold = f + ".gold";
			String simplified = f+".simp";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH, LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(poFilter, rd, null);
			RoundTripUtils.writeOut(events, gold, "UTF-8");
			RoundTripUtils.writeSimplified(events, simplified, "UTF-8");
			FileCompare compare = new FileCompare();
			assertTrue(compare.compareFilesPerLines(simplified, gold, rd.getEncoding(), false, true));
		}
	}

	private static String[] getPoFiles() throws URISyntaxException {
		return RoundTripUtils.getTestFiles(RoundTripPoWithSimplifierIT.class, "/po/Test01.po",
				".po");
		
//		URL url = RoundTripPoWithSimplifierIT.class.getResource("/po/Test01.po");
//		File dir = new File(url.toURI()).getParentFile();
//
//		FilenameFilter filter = new FilenameFilter() {
//			@Override
//			public boolean accept(File dir, String name) {
//				return name.endsWith(".po");
//			}
//		};
//		return dir.list(filter);
	}
}
