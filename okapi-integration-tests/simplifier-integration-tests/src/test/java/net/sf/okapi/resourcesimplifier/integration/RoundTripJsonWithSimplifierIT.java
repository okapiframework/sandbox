package net.sf.okapi.resourcesimplifier.integration;


import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.json.JSONFilter;
import net.sf.okapi.filters.json.Parameters;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripJsonWithSimplifierIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	private JSONFilter jsonFilter;
	private String[] testFileList;

	@Before
	public void setUp() throws Exception {
		jsonFilter = new JSONFilter();
		Parameters params = (Parameters) jsonFilter.getParameters();
		params.setBoolean("escapeExtendedChars", true);
		testFileList = getJsonFiles();
	}

	@After
	public void tearDown() throws Exception {
		jsonFilter.close();
	}

	@Test
	public void roundTripJsonFiles() throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);		
			String original = f;
			String simplified = f+".simp";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(jsonFilter, rd, null);
			RoundTripUtils.writeSimplified(events, simplified, rd.getEncoding());
			FileCompare compare = new FileCompare();
			assertTrue(compare.compareFilesPerLines(
					simplified, // out 
					original, // gold
					rd.getEncoding(), false, true));
		}
	}

	private static String[] getJsonFiles() throws URISyntaxException {
		return RoundTripUtils.getTestFiles(RoundTripJsonWithSimplifierIT.class, "/json/test01.json",
				".json");
//		// read all files in the test xml directory
//		URL url = RoundTripJsonWithSimplifierIT.class.getResource("/json/test01.json");
//		File dir = new File(url.toURI()).getParentFile();
//
//		FilenameFilter filter = new FilenameFilter() {
//			@Override
//			public boolean accept(File dir, String name) {
//				return name.endsWith(".json");
//			}
//		};
//		return dir.list(filter);
	}
}
