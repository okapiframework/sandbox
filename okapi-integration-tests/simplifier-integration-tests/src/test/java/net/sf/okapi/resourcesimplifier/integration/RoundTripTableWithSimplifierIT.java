package net.sf.okapi.resourcesimplifier.integration;


import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.table.csv.CommaSeparatedValuesFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripTableWithSimplifierIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	private CommaSeparatedValuesFilter tableFilter;
	private String[] testFileList;

	@Before
	public void setUp() throws Exception {
		tableFilter = new CommaSeparatedValuesFilter();
		testFileList = getTableFiles();
	}

	@After
	public void tearDown() throws Exception {
		tableFilter.close();
	}

	@Test
	public void roundTripTableFiles() throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);		
			String original = f;
			String gold = f + ".gold";
			String simplified = f+".simp";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH, LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(tableFilter, rd, null);
			RoundTripUtils.writeOut(events, gold, rd.getEncoding());
			RoundTripUtils.writeSimplified(true, events, null, simplified, rd.getEncoding());
			FileCompare compare = new FileCompare();
			assertTrue(compare.compareFilesPerLines(simplified, gold, rd.getEncoding()));
		}
	}

	private static String[] getTableFiles() throws URISyntaxException {
		return RoundTripUtils.getTestFiles(RoundTripTableWithSimplifierIT.class, "/table/test2cols.csv",
				".csv");
		
//		URL url = RoundTripTableWithSimplifierIT.class.getResource("/table/test2cols.csv");
//		File dir = new File(url.toURI()).getParentFile();
//
//		FilenameFilter filter = new FilenameFilter() {
//			@Override
//			public boolean accept(File dir, String name) {
//				return name.endsWith(".csv");
//			}
//		};
//		return dir.list(filter);
	}
}
