package net.sf.okapi.resourcesimplifier.integration;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import net.sf.okapi.common.ClassUtil;
import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.StreamUtil;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.exceptions.OkapiIOException;
import net.sf.okapi.common.filterwriter.XLIFFWriter;
import net.sf.okapi.common.filterwriter.XLIFFWriterParameters;
import net.sf.okapi.common.resource.simplifier.ResourceSimplifier;
import net.sf.okapi.common.skeleton.GenericSkeletonSimplifier;
import net.sf.okapi.common.skeleton.GenericSkeletonWriter;
import net.sf.okapi.common.skeleton.ISkeletonWriter;
import net.sf.okapi.steps.common.FilterEventsToRawDocumentStep;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class RoundTripUtils {
	private final static Logger LOGGER = LoggerFactory.getLogger(RoundTripUtils.class);
	public static List<Event> simplifyEvents(List<Event> events, ISkeletonWriter skeletonWriter, LocaleId trgLoc) {
		
		List<Event> simplifiedEvents = new ArrayList<>();
		ResourceSimplifier simplifier = new ResourceSimplifier(trgLoc);
		simplifier.setSkeletonWriter(skeletonWriter);
		simplifier.setOutputEncoding("UTF-8");
		simplifier.initialize();

		// Serialize all the events
		for (Event event : events) {
			List<Event> ses = simplifier.convertToList(event);
			for (Event s : ses) {
				if (!s.isNoop()) {
					simplifiedEvents.add(s);
				}
			}
		}
		
		return simplifiedEvents;
	}
		
	public static List<Event> simplifyEvents(List<Event> events) {
		return simplifyEvents(events, null, LocaleId.FRENCH);
	}
	
	public static List<Event> simplifyEvents(List<Event> events, LocaleId trgLoc) {
		return simplifyEvents(events, null, trgLoc);
	}
	
	public static List<Event> simplifyEvents(List<Event> events, ISkeletonWriter skeletonWriter) {
		return simplifyEvents(events, skeletonWriter, LocaleId.FRENCH);
	}
	
	public static void writeXliff(List<Event> events, String path) {
		XLIFFWriter w = new XLIFFWriter();
		XLIFFWriterParameters params = (XLIFFWriterParameters) w.getParameters();
		params.setCopySource(false);
		params.setIncludeIts(false);
		w.setOptions(LocaleId.SPANISH, "UTF-8");
		w.setOutput(path);
		for (Event event : events) {
			w.handleEvent(event);
		}
		w.close();
	}
	
	public static void writeOut(List<Event> events, String outputPath, String encoding) {
		writeOut(events, outputPath, LocaleId.ENGLISH, encoding);
	}
	
	public static void writeOut(List<Event> events, String outputPath, LocaleId trgLoc, String encoding) {
		FilterEventsToRawDocumentStep ferd = new FilterEventsToRawDocumentStep();
		ferd.setLastOutputStep(true);
		ferd.setOutputEncoding(encoding);
		ferd.setTargetLocale(trgLoc);
		ferd.setOutputURI(Util.toURI(outputPath));
		
		for (Event event : events) {
			ferd.handleEvent(event);
		}
		ferd.destroy();
		new File(outputPath).deleteOnExit();
	}
	
	public static void writeSimplified(List<Event> events, String outputPath, String encoding) {
		writeSimplified(false, events, null, outputPath, LocaleId.ENGLISH, encoding);
	}
	
	public static void writeSimplified(boolean isMultilingual, List<Event> events, GenericSkeletonWriter writer, String outputPath, LocaleId trgLoc, String encoding) {
		FilterEventsToRawDocumentStep ferd = new FilterEventsToRawDocumentStep();
		if (writer == null) {
			writer = new GenericSkeletonWriter();
		}
		GenericSkeletonSimplifier simplifier = new GenericSkeletonSimplifier(isMultilingual, writer, trgLoc);
		ferd.setLastOutputStep(true);
		ferd.setOutputEncoding(encoding);
		ferd.setTargetLocale(trgLoc);
		ferd.setOutputURI(Util.toURI(outputPath));
		
		for (Event event : events) {
			List<Event> simplifiedEvents = simplifier.convertToList(event);
			for (Event s : simplifiedEvents) {
				ferd.handleEvent(s);
			}
		}
		ferd.destroy();
		new File(outputPath).deleteOnExit();
	}
	
	public static void writeSimplified(boolean isMultilingual, List<Event> events, GenericSkeletonWriter writer, String outputPath, String encoding) {
		FilterEventsToRawDocumentStep ferd = new FilterEventsToRawDocumentStep();
		GenericSkeletonSimplifier simplifier = new GenericSkeletonSimplifier(isMultilingual, writer, LocaleId.ENGLISH);
		ferd.setLastOutputStep(true);
		ferd.setOutputEncoding(encoding);
		ferd.setTargetLocale(LocaleId.ENGLISH);
		ferd.setOutputURI(Util.toURI(outputPath));
		
		for (Event event : events) {
			List<Event> simplifiedEvents = simplifier.convertToList(event);
			for (Event s : simplifiedEvents) {
				ferd.handleEvent(s);
			}
		}
		ferd.destroy();
	}
	
	public static String getRoot(Class<?> testClass, String resPath, String[] files) {
		String root = null;
		File dir;
		if (ClassUtil.isInJar(testClass, resPath)) {
			// JAR
			try {
				LOGGER.trace("{} !!! JAR !!!", testClass.getName());
				String rootPath = ClassUtil.getResourceParent(testClass, resPath);
				JarFile jarFile = new JarFile(rootPath);
				try {
					final Enumeration<JarEntry> entries = jarFile.entries();
			        while (entries.hasMoreElements()) {
			            final JarEntry entry = entries.nextElement();
			            final String name = entry.getName();
			            
			            for (String file : files) {
							if (name.endsWith(file)) {
								// Unzip the test file
				            	LOGGER.trace("Temp for: {}", name);				            	
				            	File tempFile = new File(Util.buildPath(Util.getTempDirectory(), name));
				            	String temp = tempFile.getAbsolutePath();
				            	tempFile.deleteOnExit();
				            	StreamUtil.copy(jarFile.getInputStream(entry), tempFile);			            	
				            	LOGGER.trace(temp);
							}
						}
			        }
				} finally {
					jarFile.close();
				}
				
			} catch (IOException e) {
				throw new OkapiIOException(e);
			}
			
			root = Util.buildPath(Util.getTempDirectory(), resPath);
			dir = new File(root).getParentFile();			
			root = dir.getAbsolutePath();
		}
		else {
			// FS
			root = ClassUtil.getResourceParent(testClass, resPath);
		}
		return Util.ensureSeparator(root, false);
	}
	
	public static String[] getTestFiles(Class<?> testClass, String resPath, final String... extensions) throws URISyntaxException {
		String root;
		File dir;
		// jar file
		if (ClassUtil.isInJar(testClass, resPath)) {
			try {
				LOGGER.trace("{} !!! JAR !!!", testClass.getName());
				String rootPath = ClassUtil.getResourceParent(testClass, resPath);
				JarFile jarFile = new JarFile(rootPath);
				try {
					final Enumeration<JarEntry> entries = jarFile.entries();
			        while (entries.hasMoreElements()) {
			            final JarEntry entry = entries.nextElement();
			            final String name = entry.getName();
			            
			            for (String ext : extensions) {
							if (name.endsWith(ext)) {
								// Unzip the test file
				            	LOGGER.trace("Temp for: {}", name);
				            	
				            	File tempFile = new File(Util.buildPath(Util.getTempDirectory(), name));
				            	String temp = tempFile.getAbsolutePath();
				            	tempFile.deleteOnExit();
				            	StreamUtil.copy(jarFile.getInputStream(entry), tempFile);
				            	
				            	LOGGER.trace(temp);
							}
						}
			        }
				} finally {
					jarFile.close();
				}
				
			} catch (IOException e) {
				throw new OkapiIOException(e);
			}
			
			root = Util.buildPath(Util.getTempDirectory(), resPath);
			dir = new File(root).getParentFile();			
			root = dir.getAbsolutePath();
		}
		else {
			// file system
			LOGGER.trace("{} !!! FS !!!", testClass.getName());
			// read all files in the test directory
			root = ClassUtil.getResourceParent(testClass, resPath);
			dir = new File(root);			
		}
		
		// Common part
		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				for (String ext : extensions) {
					if (name.endsWith(ext)) return true;
				}
				return false;
			}
		};
		
		String[] res = dir.list(filter);
		for (int i = 0; i < res.length; i++) {
			res[i] = Util.buildPath(root, res[i]); 
		}
		return res;
	}
}
