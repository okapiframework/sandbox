package net.sf.okapi.resourcesimplifier.integration;


import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import net.sf.okapi.filters.xliff.XLIFFSkeletonWriter;

import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripXliffWithSimplifierIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	private XLIFFFilter xliffFilter;
	private String[] testFileList;
	private String root;
	private XLIFFSkeletonWriter xliffSkelWriter;

	@Before
	public void setUp() throws Exception {
		xliffFilter = new XLIFFFilter();
		testFileList = getXliffFiles();
		URL url = RoundTripXliffWithSimplifierIT.class.getResource("/xliff/lqiTest.xlf");
		root = Util.getDirectoryName(url.toURI().getPath()) + File.separator;
		
		xliffSkelWriter = new XLIFFSkeletonWriter();
		net.sf.okapi.filters.xliff.Parameters params = xliffSkelWriter.getParams();
		params.setAllowEmptyTargets(true);
		params.setIncludeExtensions(false);
		params.setIncludeIts(false);
	}

	@After
	public void tearDown() throws Exception {
		xliffFilter.close();
	}

	//@Test
	// XXX The test is replaced with 
	// net.sf.okapi.resourcesimplifier.integration.doublex.DoubleExtractionWithSimplifier#testXliffFilter()
	public void roundTripXliffFiles() throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);		
			String original = root + f;
			String out = root + f+".out";
			String simplified = root+f+".simp";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH, LocaleId.SPANISH);
			List<Event> events = FilterTestDriver.getEvents(xliffFilter, rd, null);
			RoundTripUtils.writeXliff(events, out);
			RoundTripUtils.writeSimplified(true, events, xliffSkelWriter, simplified, rd.getEncoding());
			FileCompare compare = new FileCompare();
			assertTrue(compare.compareFilesPerLines(simplified, out, rd.getEncoding(), true, false));
//			assertTrue(compare.compareFilesPerLines(out, original, rd.getEncoding(), true, false));
		}
	}

	private static String[] getXliffFiles() throws URISyntaxException {
		// read all files in the test xml directory
		URL url = RoundTripXliffWithSimplifierIT.class.getResource("/xliff/lqiTest.xlf");
		File dir = new File(url.toURI()).getParentFile();

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".xlf");
			}
		};
		return dir.list(filter);
	}
}
