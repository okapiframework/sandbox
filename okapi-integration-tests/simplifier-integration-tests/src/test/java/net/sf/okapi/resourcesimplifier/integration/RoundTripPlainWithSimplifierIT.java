package net.sf.okapi.resourcesimplifier.integration;


import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.plaintext.PlainTextFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripPlainWithSimplifierIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	private PlainTextFilter plainTextFilter;
	private String[] testFileList;

	@Before
	public void setUp() throws Exception {
		plainTextFilter = new PlainTextFilter();
		testFileList = getPlainTextFiles();
	}

	@After
	public void tearDown() throws Exception {
		plainTextFilter.close();
	}

	@Test
	public void roundTripPlainTextFiles() throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);		
			String original = f;
			String simplified = f+".simp";
			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(plainTextFilter, rd, null);
			RoundTripUtils.writeSimplified(events, simplified, rd.getEncoding());
			FileCompare compare = new FileCompare();
			assertTrue(compare.compareFilesPerLines(original, simplified, rd.getEncoding()));
		}
	}

	private static String[] getPlainTextFiles() throws URISyntaxException {
		return RoundTripUtils.getTestFiles(RoundTripPlainWithSimplifierIT.class, "/plaintext/lgpl.txt",
				".txt");
		
//		// read all files in the test xml directory
//		URL url = RoundTripPlainWithSimplifierIT.class.getResource("/plaintext/lgpl.txt");
//		File dir = new File(url.toURI()).getParentFile();
//
//		FilenameFilter filter = new FilenameFilter() {
//			@Override
//			public boolean accept(File dir, String name) {
//				return name.endsWith(".txt");
//			}
//		};
//		return dir.list(filter);
	}
}
