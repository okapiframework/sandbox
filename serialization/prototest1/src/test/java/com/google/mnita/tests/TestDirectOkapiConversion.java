package com.google.mnita.tests;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filterwriter.XLIFFWriter;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.filters.json.JSONFilter;
import net.sf.okapi.filters.json.Parameters;

@RunWith(JUnit4.class)
public class TestDirectOkapiConversion extends BaseTestClass {

	@Test
	public void test() throws IOException {
		try (InputStream resourceAsStream = this.getClass().getResourceAsStream("/" + INPUT_FILE);
				RawDocument doc = new RawDocument(resourceAsStream, DEFAULT_CHARSET, SOURCE_LOCALE);
				//PropertiesFilter filter = new net.sf.okapi.filters.properties.PropertiesFilter();
				//HtmlFilter filter = new net.sf.okapi.filters.html.HtmlFilter();
				JSONFilter filter = new net.sf.okapi.filters.json.JSONFilter();
				XLIFFWriter fo = new XLIFFWriter()) {

			FilterConfigurationMapper mapper = new FilterConfigurationMapper();
	        mapper.addConfigurations(HtmlFilter.class.getName());
	        filter.setFilterConfigurationMapper(mapper);

			net.sf.okapi.filters.json.Parameters params = (Parameters) filter.getParameters();
			params.setSubfilter("okf_html");
			filter.setParameters(params);

			fo.setOutput(OUTPUT_DIR + File.separator + OUTPUT_FILE_ORG);
			fo.setOptions(TARGET_LOCALE, DEFAULT_CHARSET);

			filter.open(doc);
			while (filter.hasNext()) {
				fo.handleEvent(filter.next());
			}
		}
	}
}
