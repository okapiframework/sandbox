package com.google.mnita.tests;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Assert;

import net.sf.okapi.common.LocaleId;

abstract public class BaseTestClass {
	static final LocaleId SOURCE_LOCALE = LocaleId.ENGLISH;
	static final LocaleId TARGET_LOCALE = LocaleId.FRENCH;
	static final String DEFAULT_CHARSET = "UTF-8";
	static final String OUTPUT_DIR = "dataOut";
	// static final String INPUT_FILE = "demo.properties";
	// static final String INPUT_FILE = "index.html";
	static final String INPUT_FILE = "test07-subfilter.json";
	static final String OUTPUT_FILE_ORG = "directOkapi.xlf";

	public static void assertFilesContentsEqual(String f1, String f2) {
		try {
			byte[] allBytes1 = Files.readAllBytes(Paths.get(OUTPUT_DIR, f1));
			byte[] allBytes2 = Files.readAllBytes(Paths.get(OUTPUT_DIR, f2));
			org.junit.Assert.assertArrayEquals(allBytes1, allBytes2);
		} catch (IOException ex) {
			Assert.fail("problem reading files");
		}
	}
}
