package com.google.mnita.tests;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.filters.DefaultFilters;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filterwriter.XLIFFWriter;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.filters.json.JSONFilter;
import net.sf.okapi.filters.json.Parameters;

@RunWith(JUnit4.class)
public class TestOkapiStoredEvents extends BaseTestClass {
	static final String OUTPUT_FILE = "storedOkapi.xlf";

	private ArrayList<Event> events = new ArrayList<>();

	public void parseInputFile() throws IOException {
		try (InputStream resourceAsStream = this.getClass().getResourceAsStream("/" + INPUT_FILE);
				RawDocument doc = new RawDocument(resourceAsStream, DEFAULT_CHARSET, SOURCE_LOCALE);
				//PropertiesFilter filter = new net.sf.okapi.filters.properties.PropertiesFilter()
				//HtmlFilter filter = new net.sf.okapi.filters.html.HtmlFilter()
				JSONFilter filter = new net.sf.okapi.filters.json.JSONFilter()) {

			FilterConfigurationMapper mapper = new FilterConfigurationMapper();
			DefaultFilters.setMappings(mapper, false, true);

			mapper.addConfigurations(HtmlFilter.class.getName());
			filter.setFilterConfigurationMapper(mapper);

			net.sf.okapi.filters.json.Parameters params = (Parameters) filter.getParameters();
			params.setSubfilter("okf_html");
			filter.setParameters(params);
			filter.setTrgLoc(TARGET_LOCALE);

			filter.open(doc);
			while (filter.hasNext()) {
				Event event = filter.next();
				events.add(event);
			}
		}
	}

	void generateOutputFile() {
		try (XLIFFWriter fo = new XLIFFWriter()) {
			fo.setOutput(OUTPUT_DIR + File.separator + OUTPUT_FILE);
			fo.setOptions(TARGET_LOCALE, DEFAULT_CHARSET);

			for (Event event : events) {
				fo.handleEvent(event);
			}
		}
	}

	@Test
	public void test() throws IOException {
		parseInputFile();
		generateOutputFile();

		assertFilesContentsEqual(OUTPUT_FILE_ORG, OUTPUT_FILE);
	}
}
