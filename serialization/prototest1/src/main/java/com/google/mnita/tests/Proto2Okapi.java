package com.google.mnita.tests;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IResource;
import net.sf.okapi.common.ISkeleton;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.EndSubfilter;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.INameable;
import net.sf.okapi.common.resource.IReferenceable;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.StartGroup;
import net.sf.okapi.common.resource.StartSubfilter;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.resource.TextPart;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.common.skeleton.GenericSkeleton;

public class Proto2Okapi {

	static TagType toTagType(net.sf.okapi.proto.TextFragment.TagType tt) {
		switch (tt) {
			case CLOSING: return TagType.CLOSING;
			case OPENING: return TagType.OPENING;
			case PLACEHOLDER: return TagType.PLACEHOLDER;
			default: return TagType.valueOf("ERROR");
		}
	}

	static Code toCode(net.sf.okapi.proto.Code code) {
		Code result = new Code(toTagType(code.getTagType()), code.getType(), code.getData());
		result.setId(code.getId());
		result.setOuterData(code.getOuterData());
		if (null != code.getOriginalId()) {
			result.setOriginalId(code.getOriginalId());
		}
		//map<string, string> annotations = 8;
		return result;
	}

	static TextFragment toTextFragment(net.sf.okapi.proto.TextFragment tf) {
		List<Code> codes = new ArrayList<>();
		for (net.sf.okapi.proto.Code code : tf.getCodesList()) {
			codes.add(toCode(code));
		}
		TextFragment result = new TextFragment(tf.getText(), codes);

		return result;
	}

	static TextPart toTextPart(net.sf.okapi.proto.TextPart tp) {
		TextPart result = new TextPart(toTextFragment(tp.getText()));
		return result;
	}

	static TextContainer toTextContainer(net.sf.okapi.proto.TextContainer tc) {
		TextContainer result = new TextContainer();
		setINameable(result, tc.getNameable(), true);
		
		for (net.sf.okapi.proto.TextPart part : tc.getPartsList()) {
			result.append(toTextPart(part));
		}
		result.setHasBeenSegmentedFlag(tc.getSegApplied());
		
		return result;
	}

	static Property toProperty(net.sf.okapi.proto.Property prop) {
		Property result = new Property(prop.getName(), prop.getValue(), prop.getIsReadOnly());
		return result;
	}

	static void setIResource(IResource out, net.sf.okapi.proto.IResource value) {
		setIResource(out, value, false);
	}

	static void setIResource(IResource out, net.sf.okapi.proto.IResource value, boolean isTextContainer) {
		if (!isTextContainer) {
			out.setId(value.getId());
			ISkeleton newSkeleton = new GenericSkeleton(value.getSkeleton().getText());
			newSkeleton.setParent(out);
			out.setSkeleton(newSkeleton);
		}
		// TODO
//		for (String annotation : value.getAnnotationsList()) {
//			out.setAnnotation(annotation);			
//		}
	}

	static void setINameable(INameable out, net.sf.okapi.proto.INameable value) {
		setINameable(out, value, false);
	}

	static void setINameable(INameable out, net.sf.okapi.proto.INameable value, boolean isTextContainer) {
		setIResource(out, value.getResource(), isTextContainer);
		if (!isTextContainer) {
			out.setName(value.getName());
			out.setType(value.getType());
			out.setMimeType(value.getMimeType());
			out.setIsTranslatable(value.getIsTranslatable());
			out.setPreserveWhitespaces(value.getPreserveWhitespaces());
		}

		for (net.sf.okapi.proto.Property property : value.getPropertiesList()) {
			out.setProperty(toProperty(property));	
		}
	}

	static void setIReferenceable(IReferenceable out, net.sf.okapi.proto.IReferenceable value) {
		out.setReferenceCount(value.getRefCount());
	}

	static TextUnit toTextUnit(net.sf.okapi.proto.TextUnit tu) {
		TextUnit result = new TextUnit("");

		setINameable(result, tu.getNameable());
		setIReferenceable(result, tu.getReferenceable());
		
		result.setSource(toTextContainer(tu.getSource()));

		for (Entry<String, net.sf.okapi.proto.TextContainer> trg : tu.getTargets().entrySet()) {			
			result.setTarget(LocaleId.fromBCP47(trg.getKey()), toTextContainer(trg.getValue()));
		}

		return result;
	}

	static StartDocument toStartDocument(net.sf.okapi.proto.StartDocument sdoc) {
		StartDocument result = new StartDocument("");
		setINameable(result, sdoc.getNameable());
		result.setLineBreak(sdoc.getLineBreak());
		result.setLocale(LocaleId.fromBCP47(sdoc.getLocale()));
		result.setEncoding(sdoc.getEncoding(), sdoc.getHasUtf8Bom());
		result.setMultilingual(sdoc.getMultilingual());
		if (result.getName().isEmpty()) {
			result.setName("unknown");
		}

		// TODO
//		result.getFilterWriter();
//		result.setFilterWriter(new GenericFilterWriter());
//		if (null != sdoc.getParameters()) {
//			net.sf.okapi.common.filterwriter.Parameters param =
//					new net.sf.okapi.common.filterwriter.Parameters();
//			param.fromString(sdoc.getParameters());
//			result.setFilterParameters(param);
//		}

		return result;
	}

	static DocumentPart toDocumentPart(net.sf.okapi.proto.DocumentPart value) {
		DocumentPart result = new DocumentPart();

		result.setParentId(value.getParentId());
		setINameable(result, value.getNameable());
		setIReferenceable(result, value.getReferenceable());

		return result;
	}

	static StartGroup toStartGroup(net.sf.okapi.proto.StartGroup value) {
		StartGroup result = new StartGroup();

		result.setParentId(value.getParentId());
		setINameable(result, value.getNameable());
		setIReferenceable(result, value.getReferenceable());

		return result;
	}

	static StartSubfilter toStartSubfilter(net.sf.okapi.proto.StartSubfilter value) {
		// StartSubfilter result = new StartSubfilter();
		StartSubfilter result = new StartSubfilter("", toStartDocument(value.getStartDoc()), null);
		setINameable(result, value.getNameable());
		return result;
	}

	static EndSubfilter toEndSubfilter(net.sf.okapi.proto.EndSubfilter value) {
		EndSubfilter result = new EndSubfilter();
		setIResource(result, value.getResource());
		return result;
	}

	static Ending toEndDocument(net.sf.okapi.proto.EndDocument value) {
		Ending result = new Ending();
		setIResource(result, value.getResource());
		return result;
	}

	static Ending toEndGroup(net.sf.okapi.proto.EndGroup value) {
		Ending result = new Ending();
		setIResource(result, value.getResource());
		return result;
	}

	static Event toEvent(net.sf.okapi.proto.Event event) {
		Event result = new Event();

		switch (event.getResourceCase()) {
			case ENDDOCUMENT:
				Ending endDocument = toEndDocument(event.getEndDocument());
				result.setResource(endDocument);
				result.setEventType(EventType.END_DOCUMENT);
				break;
			case STARTDOCUMENT:
				StartDocument doc = toStartDocument(event.getStartDocument());
				result.setResource(doc);
				result.setEventType(EventType.START_DOCUMENT);
				break;
			case TEXTUNIT:
				TextUnit tu = toTextUnit(event.getTextUnit());
				result.setResource(tu);
				result.setEventType(EventType.TEXT_UNIT);
				break;
			case DOCUMENTPART:
				DocumentPart docPart = toDocumentPart(event.getDocumentPart());
				result.setResource(docPart);
				result.setEventType(EventType.DOCUMENT_PART);
				break;
			case STARTGROUP:
				StartGroup startGroup = toStartGroup(event.getStartGroup());
				result.setResource(startGroup);
				result.setEventType(EventType.START_GROUP);
				break;
			case ENDGROUP:
				Ending endGroup = toEndGroup(event.getEndGroup());
				result.setResource(endGroup);
				result.setEventType(EventType.END_GROUP);
				break;
			case STARTSUBFILTER:
				StartSubfilter startSubfilter = toStartSubfilter(event.getStartSubfilter());
				result.setResource(startSubfilter);
				result.setEventType(EventType.START_SUBFILTER);
				break;
			case ENDSUBFILTER:
				EndSubfilter endSubfilter = toEndSubfilter(event.getEndSubfilter());
				result.setResource(endSubfilter);
				result.setEventType(EventType.END_SUBFILTER);
				break;
			default:
				break;
		}
/*
		case START_SUBDOCUMENT:
		case END_SUBDOCUMENT:
*/
		return result;
	}
}
