package com.google.mnita.tests;

import java.util.HashMap;
import java.util.Map;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IResource;
import net.sf.okapi.common.ISkeleton;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.annotation.IAnnotation;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.EndSubfilter;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.INameable;
import net.sf.okapi.common.resource.IReferenceable;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.StartGroup;
import net.sf.okapi.common.resource.StartSubfilter;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.resource.TextPart;
import net.sf.okapi.proto.TextContainer.Builder;

public class Okapi2Proto {

	static net.sf.okapi.proto.TextFragment.TagType toTagType(TagType tt) {
		switch (tt) {
			case CLOSING: return net.sf.okapi.proto.TextFragment.TagType.CLOSING;
			case OPENING: return net.sf.okapi.proto.TextFragment.TagType.OPENING;
			case PLACEHOLDER: return net.sf.okapi.proto.TextFragment.TagType.PLACEHOLDER;
			default: return net.sf.okapi.proto.TextFragment.TagType.UNRECOGNIZED;
		}
	}

	static net.sf.okapi.proto.Code toCode(Code tp) {
		net.sf.okapi.proto.Code.Builder builder = net.sf.okapi.proto.Code.newBuilder()
				.setTagType(toTagType(tp.getTagType()))
				.setId(tp.getId())
				.setType(tp.getType())
				.setData(tp.getData())
				.setOuterData(tp.getOuterData());
				// .setFlag(value) ???
				// .putAllAnnotations(values)

		if (null != tp.getOriginalId()) {
			builder.setOriginalId(tp.getOriginalId());
		}
		//map<string, string> annotations = 8;

		return builder.build();
	}

	static net.sf.okapi.proto.TextFragment toTextFragment(TextFragment tf) {
		net.sf.okapi.proto.TextFragment.Builder builder = net.sf.okapi.proto.TextFragment.newBuilder()
				.setText(tf.getCodedText());

		for (Code code : tf.getCodes()) {
			builder.addCodes(toCode(code));
		}

		return builder.build();
	}

	static net.sf.okapi.proto.TextPart toTextPart(TextPart tp) {
		net.sf.okapi.proto.TextPart.Builder builder = net.sf.okapi.proto.TextPart.newBuilder()
				.setIsSegment(tp.isSegment())
				.setText(toTextFragment(tp.getContent()));
		return builder.build();
	}

	static net.sf.okapi.proto.TextContainer toTextContainer(TextContainer tc) {
		Builder builder = net.sf.okapi.proto.TextContainer.newBuilder()
				.setNameable(toINameable(tc, true))
				.setSegApplied(tc.hasBeenSegmented());

		for (TextPart part : tc.getParts()) {
			builder.addParts(toTextPart(part));
		}

		return builder.build();
	}

	static net.sf.okapi.proto.ISkeleton toISkeleton(ISkeleton sk) {
		net.sf.okapi.proto.ISkeleton.Builder builder = net.sf.okapi.proto.ISkeleton.newBuilder();
		if (sk != null) {
			builder.setText(sk.toString());
		}
		return builder.build();
	}

	static net.sf.okapi.proto.Property toProperty(Property prop) {
		net.sf.okapi.proto.Property.Builder builder = net.sf.okapi.proto.Property.newBuilder()
				.setName(prop.getName())
				.setValue(prop.getValue())
				.setIsReadOnly(prop.isReadOnly());

		return builder.build();
	}

	static net.sf.okapi.proto.IResource toIResource(IResource value) {
		return toIResource(value, false);
	}

	static net.sf.okapi.proto.IResource toIResource(IResource value, boolean isTextContainer) {
		net.sf.okapi.proto.IResource.Builder builder = net.sf.okapi.proto.IResource.newBuilder();
		if (!isTextContainer) {
			// for getId and getSkeleton TextContainer throws
			//     java.lang.UnsupportedOperationException:
			//         This method is intentionally not implemented for TextContainer.
			builder.setId(value.getId())
				.setSkeleton(toISkeleton(value.getSkeleton()));
		}

		for (IAnnotation annotation : value.getAnnotations()) {
			builder.addAnnotations(annotation.toString());
		}

		return builder.build();
	}

	static net.sf.okapi.proto.INameable toINameable(INameable value) {
		return toINameable(value, false);
	}

	static net.sf.okapi.proto.INameable toINameable(INameable value, boolean isTextContainer) {
		net.sf.okapi.proto.INameable.Builder builder = net.sf.okapi.proto.INameable.newBuilder()
				.setResource(toIResource(value, isTextContainer))
				;

		if (!isTextContainer) {
			builder.setIsTranslatable(value.isTranslatable())
				.setPreserveWhitespaces(value.preserveWhitespaces());
			if (value.getType() != null) {
				builder.setType(value.getType());
			}
		}

		if (value.getMimeType() != null) {
			builder.setMimeType(value.getMimeType());
		}

		if (value.getName() != null) {
			builder.setName(value.getName());
		}

		for (String propName : value.getPropertyNames()) {
			builder.addProperties(toProperty(value.getProperty(propName)));
		}
		return builder.build();
	}

	static net.sf.okapi.proto.IReferenceable toIReferenceable(IReferenceable value) {
		net.sf.okapi.proto.IReferenceable.Builder builder = net.sf.okapi.proto.IReferenceable.newBuilder()
				.setRefCount(value.getReferenceCount());

		return builder.build();
	}

	static net.sf.okapi.proto.TextUnit toTextUnit(ITextUnit tu) {
		net.sf.okapi.proto.TextUnit.Builder builder = net.sf.okapi.proto.TextUnit.newBuilder()
				.setNameable(toINameable(tu))
				.setReferenceable(toIReferenceable(tu))
				.setSource(toTextContainer(tu.getSource()))
				;

		Map<String, net.sf.okapi.proto.TextContainer> targets = new HashMap<>();
		for (LocaleId locale : tu.getTargetLocales()) {
			targets.put(locale.toBCP47(), toTextContainer(tu.getTarget(locale)));
		}
		builder.putAllTargets(targets);

		return builder.build();
	}

	static net.sf.okapi.proto.StartDocument toStartDocument(StartDocument sdoc) {
		net.sf.okapi.proto.StartDocument.Builder builder = net.sf.okapi.proto.StartDocument.newBuilder()
				.setNameable(toINameable(sdoc))
				.setEncoding(sdoc.getEncoding())
				.setLineBreak(sdoc.getLineBreak())
				.setLocale(sdoc.getLocale().toBCP47())
				.setHasUtf8Bom(sdoc.hasUTF8BOM())
				.setMultilingual(sdoc.isMultilingual())
				;
		if (null != sdoc.getFilterParameters()) {
			builder.setParameters(sdoc.getFilterParameters().toString());
		}

		return builder.build();
	}

	static net.sf.okapi.proto.EndDocument toEndDocument(Ending value) {
		net.sf.okapi.proto.EndDocument.Builder builder = net.sf.okapi.proto.EndDocument.newBuilder()
				.setResource(toIResource(value))
				;
		return builder.build();
	}

	static net.sf.okapi.proto.EndGroup toEndGroup(Ending value) {
		net.sf.okapi.proto.EndGroup.Builder builder = net.sf.okapi.proto.EndGroup.newBuilder()
				.setResource(toIResource(value))
				;
		return builder.build();
	}

	static net.sf.okapi.proto.DocumentPart toDocumentPart(DocumentPart value) {
		net.sf.okapi.proto.DocumentPart.Builder builder = net.sf.okapi.proto.DocumentPart.newBuilder()
				.setNameable(toINameable(value))
				.setReferenceable(toIReferenceable(value));

		String parentId = value.getParentId();
		if (null != parentId)
			builder.setParentId(parentId);

		return builder.build();
	}

	static net.sf.okapi.proto.StartGroup toStartGroup(StartGroup value) {
		net.sf.okapi.proto.StartGroup.Builder builder = net.sf.okapi.proto.StartGroup.newBuilder()
				.setNameable(toINameable(value))
				.setReferenceable(toIReferenceable(value));

		String parentId = value.getParentId();
		if (null != parentId)
			builder.setParentId(parentId);

		return builder.build();
	}

	static net.sf.okapi.proto.StartSubfilter toStartSubfilter(StartSubfilter value) {
		net.sf.okapi.proto.StartSubfilter.Builder builder = net.sf.okapi.proto.StartSubfilter.newBuilder()
				.setNameable(toINameable(value))
				.setStartDoc(toStartDocument(value.getStartDoc()))
				;
		return builder.build();
	}

	static net.sf.okapi.proto.EndSubfilter toEndSubfilter(EndSubfilter value) {
		net.sf.okapi.proto.EndSubfilter.Builder builder = net.sf.okapi.proto.EndSubfilter.newBuilder()
				.setResource(toIResource(value))
				;
		return builder.build();
	}

/*
	static StartGroup toStartGroup(net.sf.okapi.proto.StartGroup value) {
		StartGroup result = new StartGroup();

	static StartSubfilter toStartSubfilter(net.sf.okapi.proto.StartSubfilter value) {
		StartSubfilter result = new StartSubfilter();
		return result;
	}

	static EndSubfilter toEndSubfilter(net.sf.okapi.proto.EndSubfilter value) {
		EndSubfilter result = new EndSubfilter();
		return result;
	}
*/

	static net.sf.okapi.proto.Event toEvent(Event event) {
		net.sf.okapi.proto.Event.Builder builder = net.sf.okapi.proto.Event.newBuilder();

		if (event.isStartDocument()) {
			builder.setStartDocument(toStartDocument(event.getStartDocument()));
		} else if (event.isEndDocument()) {
			builder.setEndDocument(toEndDocument(event.getEnding()));
		} else if (event.isTextUnit()) {
			builder.setTextUnit(toTextUnit(event.getTextUnit()));
		} else if (event.isDocumentPart()) {
			builder.setDocumentPart(toDocumentPart(event.getDocumentPart()));
		} else if (event.isStartGroup()) {
			builder.setStartGroup(toStartGroup(event.getStartGroup()));
		} else if (event.isEndGroup()) {
			builder.setEndGroup(toEndGroup(event.getEnding()));
		} else if (event.isStartSubfilter()) {
			builder.setStartSubfilter(toStartSubfilter(event.getStartSubfilter()));
		} else if (event.isEndSubfilter()) {
			builder.setEndSubfilter(toEndSubfilter(event.getEndSubfilter()));
		}

		return builder.build();
	}
}
