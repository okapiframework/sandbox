syntax = "proto3";

option java_package = "net.sf.okapi.proto";
option java_outer_classname  = "OkapiProtoMapping";
option java_multiple_files = true;

// option optimize_for = LITE_RUNTIME;
// option optimize_for = CODE_SIZE;

// TextUnit implements ITextUnit
//   ITextUnit extends INameable, IReferenceable
//     INameable extends IResource
// StartDocument extends BaseNameable
//   BaseNameable implements INameable
// Ending implements IResource

message IResource {
	string          id          = 1;
	ISkeleton       skeleton    = 2;
	repeated string annotations = 3;
}

message INameable {
	IResource         resource            = 1;
	string            name                = 2;
	string            type                = 3;
	string            mimeType            = 4;
	bool              isTranslatable      = 5;
	bool              preserveWhitespaces = 6;
	repeated Property properties          = 7;
}

message IReferenceable {
	int32 refCount = 1;
}

message TextUnit {
	INameable                  nameable      = 1;
	IReferenceable             referenceable = 2;
	TextContainer              source        = 3;
	map<string, TextContainer> targets       = 4;
//	VariantSources  variantSources
//	AlignedSegments alignedSegments
}

message Property {
	enum PropName {
		ENCODING         =  0;
		LANGUAGE         =  1;
		APPROVED         =  2;
		NOTE             =  3;
		TRANSNOTE        =  4;
		COORDINATES      =  5;
		STATE_QUALIFIER  =  6;
		STATE            =  7;
		ITS_LQI          =  8;
		ITS_PROV         =  9;
		ITS_MTCONFIDENCE = 10;
		XLIFF_TOOL       = 11;
		XLIFF_PHASE      = 12;
	}

	// PropName name        = 1;
	string name             = 1;
	string value            = 2;
	bool isReadOnly         = 3;
	Annotations annotations = 4;
}

message Annotations {
	// class_name => annotation
	map<string,string> annotations = 1;
}

// TextContainer implements INameable, Iterable<TextPart>
message TextContainer {
	INameable nameable            = 1;
	repeated TextPart parts       = 2;
	bool              segApplied  = 3;
	Segments          segments    = 4;
}

message Segments {
	enum AlignmentStatus {
		ALIGNED     = 0;
		NOT_ALIGNED = 1;
	}

	AlignmentStatus alignmentStatus = 1;
	TextContainer parent            = 2;
	repeated TextPart parts         = 3;
}

message TextPart {
	bool isSegment    = 1;
	TextFragment text = 2;
}

message TextFragment {
	enum MarkerType {
		MARKER_OPENING  = 0;
		MARKER_CLOSING  = 1;
		MARKER_ISOLATED = 2;
	}

	enum RefMarkerType {
		REFMARKER_START = 0;
		REFMARKER_END   = 1;
		REFMARKER_SEP   = 2;
	}

	enum TagType {
		OPENING     = 0;
		CLOSING     = 1;
		PLACEHOLDER = 2;
	}

	string text         = 1;
	repeated Code codes = 2;
	bool isBalanced     = 3;
	int32 lastCodeID    = 4;
}

message Code {
	enum CodeType {
		TYPE_NULL                       =  0;
		TYPE_BOLD                       =  1;
		TYPE_ITALIC                     =  2;
		TYPE_UNDERLINED                 =  3;
		TYPE_LB                         =  4;
		TYPE_LINK                       =  5;
		TYPE_IMAGE                      =  6;
		TYPE_COMMENT                    =  7;
		TYPE_CDATA                      =  8;
		TYPE_XML_PROCESSING_INSTRUCTION =  9;
		TYPE_REFERENCE                  = 10;
		TYPE_ANNOTATION_ONLY            = 11;
	}

	enum Flags {
		HASREF     = 0;
		CLONEABLE  = 1;
		DELETEABLE = 2;
	}

	TextFragment.TagType tagType    = 1;
	string originalId               = 2;
	int32 id                        = 3;
	string type                     = 4;
	string data                     = 5;
	string outerData                = 6;
	int32 flag                      = 7;
	map<string, string> annotations = 8;
}

message ISkeleton {
	string text = 1;
	// implementing ISkeleton:
	//   okapi\core\src\main\java\net\sf\okapi\common\skeleton\GenericSkeleton.java
	//   okapi\filters\icml\src\main\java\net\sf\okapi\filters\icml\ICMLSkeleton.java
	//   okapi\filters\idml\src\main\java\net\sf\okapi\filters\idml\IDMLSkeleton.java
	//   okapi\filters\openxml\src\main\java\net\sf\okapi\filters\openxml\BlockSkeleton.java
	//   okapi\filters\openxml\src\main\java\net\sf\okapi\filters\openxml\MarkupSkeleton.java
	//   okapi\filters\openxml\src\main\java\net\sf\okapi\filters\openxml\StringItemSkeleton.java
	//   okapi\filters\xliff2\src\main\java\net\sf\okapi\filters\xliff2\XMLSkeleton.java
}

message StartDocument {
	INameable nameable     = 1;
	string    lineBreak    = 2;
	string    locale       = 3;
	string    encoding     = 4;
	bool      multilingual = 5;
	bool      hasUtf8Bom   = 6;
	string    parameters   = 7;
// TODO
//	protected IFilterWriter filterWriter;
}

// EndDocument extends Ending
message EndDocument {
	IResource resource = 1;
}

// EndGroup extends Ending
message EndGroup {
	IResource resource = 1;
}

// DocumentPart extends BaseReferenceable
// BaseReferenceable extends BaseNameable implements IReferenceable
// BaseNameable implements INameable
message DocumentPart {
	string parentId = 2; // from BaseReferenceable
	INameable nameable = 3;
	IReferenceable referenceable = 4;
}

// StartGroup extends BaseReferenceable
// BaseReferenceable extends BaseNameable implements IReferenceable
// BaseNameable implements INameable
message StartGroup {
	string parentId = 2; // from BaseReferenceable
	INameable nameable = 3;
	IReferenceable referenceable = 4;
}

// StartSubfilter extends StartGroup
message StartSubfilter {
	INameable nameable = 1;
	StartDocument startDoc = 2;
	// SubFilterSkeletonWriter skelWriter;
	// IEncoder parentEncoder;
}

// EndSubfilter extends Ending
message EndSubfilter {
	IResource resource = 1;
}

message Event {
	oneof resource {
		TextUnit textUnit             = 1;
		StartDocument startDocument   = 2;
		EndDocument endDocument       = 3;
		DocumentPart documentPart     = 4;
		StartGroup startGroup         = 5;
		EndGroup endGroup             = 6;
		StartSubfilter StartSubfilter = 7;
		EndSubfilter endSubfilter     = 8;
	}
}
