/*===========================================================================
Copyright (C) 2009-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
This library is free software; you can redistribute it and/or modify it 
under the terms of the GNU Lesser General Public License as published by 
the Free Software Foundation; either version 2.1 of the License, or (at 
your option) any later version.

This library is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
General Public License for more details.

You should have received a copy of the GNU Lesser General Public License 
along with this library; if not, write to the Free Software Foundation, 
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.concurrent;

import java.util.concurrent.Executor;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.pipeline.ICallableStep;
import net.sf.okapi.common.pipeline.IPipelineStep;

/**
 * Threaded pipeline step {@link IPipelineStep}. When submitted to an {@link Executor} will
 * process one {@link Event}.
 */
class CallableStep implements ICallableStep<SortableEvent> {
	private IPipelineStep step;
	private SortableEvent currentEvent;
	
	public CallableStep(IPipelineStep step) {
		this.step = step;
	}
	
	@Override
	public Event processNow(Event event) {
		return step.handleEvent(event);
	}
	
	@Override
	public SortableEvent call() throws Exception {
		Event e = step.handleEvent(currentEvent);
		return new SortableEvent(e, currentEvent.getOrder());
	}

	@Override
	public IParameters getParameters() {
		return step.getParameters();
	}

	@Override
	public void setParameters(IParameters params) {
		step.setParameters(params);
	}

	@Override
	public String getName() {
		return String.format("%s (Callable)", step.getName());
	}

	@Override
	public String getDescription() {
		return String.format("Callable Wrapper for: %s", step.getName());
	}

	@Override
	public String getHelpLocation() {
		return step.getHelpLocation();
	}

	@Override
	public Event handleEvent(Event event) {
		currentEvent = (SortableEvent)event;
		return currentEvent;
	}
	
	@Override
	public boolean isDone() {
		return step.isDone();
	}

	@Override
	public void destroy() {
		step.destroy();
	}

	@Override
	public void cancel() {
		step.cancel();
	}

	@Override
	public boolean isLastOutputStep() {
		return step.isLastOutputStep();
	}

	@Override
	public void setLastOutputStep(boolean isLastStep) {
		step.setLastOutputStep(isLastStep);
	}

	@Override
	public IPipelineStep getMainStep() {
		return step;
	}
}
