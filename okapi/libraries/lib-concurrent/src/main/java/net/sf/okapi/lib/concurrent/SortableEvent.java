/*===========================================================================
  Copyright (C) 2009-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.concurrent;

import net.sf.okapi.common.Event;

class SortableEvent extends Event implements Comparable<SortableEvent> {
	private int order;

	public SortableEvent(Event event, int order) {
		super(event.getEventType(), event.getResource());
		this.setOrder(order);
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	public int compareTo(SortableEvent o) {
		return Double.compare(o.getOrder(), getOrder());
	}

	@Override
	/**
	 * Since we are driving from a concrete class the equals is not perfectly correct
	 * but should serve the purpose of comparing based on order
	 */
	public boolean equals(Object aThat) {
	    //check for self-comparison
	    if (this == aThat) return true;

	    if (!(aThat instanceof SortableEvent)) return false;

	    SortableEvent that = (SortableEvent)aThat;

	    return (compareTo(that) == 0);	     
	  }

	@Override
	public int hashCode() {
		return order;
	}
}
