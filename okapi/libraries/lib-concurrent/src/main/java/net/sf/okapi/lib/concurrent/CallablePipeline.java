/*===========================================================================
  Copyright (C) 2009-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.concurrent;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.pipeline.IPipeline;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.common.pipeline.Pipeline;
import net.sf.okapi.common.pipeline.PipelineReturnValue;
import net.sf.okapi.common.resource.RawDocument;

/**
 * Threaded pipeline {@link IPipeline}. When submitted to an {@link Executor} will
 * process all events till completion.
 */
class CallablePipeline implements IPipeline, Callable<IPipeline> {
	private Event startEvent;
	private IPipeline pipeline; 
	
	public CallablePipeline() {
		this.pipeline = new Pipeline();
	}
	
	public CallablePipeline(IPipeline pipeline) {
		this.pipeline = pipeline;	
	}
	
	@Override
	public IPipeline call() throws Exception {		
		pipeline.process(this.startEvent);
		return this;
	}
	
	@Override
	public void process(Event input) {
		this.startEvent = input;
	}

	@Override
	public void process(RawDocument input) {
		this.startEvent = new Event(EventType.RAW_DOCUMENT, input); 
		
	}

	@Override
	public PipelineReturnValue getState() {
		return pipeline.getState();
	}

	@Override
	public void cancel() {
		pipeline.cancel();
	}

	@Override
	public void addStep(IPipelineStep step) {
		pipeline.addStep(step);
	}

	@Override
	public List<IPipelineStep> getSteps() {
		return pipeline.getSteps();
	}

	@Override
	public void startBatch() {
		pipeline.startBatch();
	}

	@Override
	public void endBatch() {
		pipeline.endBatch();
	}

	@Override
	public void destroy() {
		pipeline.destroy();
	}

	@Override
	public void clearSteps() {
		pipeline.clearSteps();
	}

	@Override
	public void setId(String id) {
		pipeline.setId(id);
	}

	@Override
	public String getId() {
		return pipeline.getId();
	}
}
