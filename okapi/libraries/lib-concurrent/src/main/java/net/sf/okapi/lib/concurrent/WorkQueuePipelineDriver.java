/*===========================================================================
  Copyright (C) 2009-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.concurrent;

import java.util.List;
import java.util.Stack;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.pipeline.IPipeline;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.common.pipeline.IWorkQueueStep;
import net.sf.okapi.common.pipeline.Pipeline;
import net.sf.okapi.common.pipeline.PipelineReturnValue;
import net.sf.okapi.common.pipelinedriver.IBatchItemContext;
import net.sf.okapi.common.pipelinedriver.IPipelineDriver;
import net.sf.okapi.common.pipelinedriver.PipelineDriver;
import net.sf.okapi.common.pipelinedriver.PipelineDriverUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Threaded implementation of the {@link IPipelineDriver} interface.
 */
public class WorkQueuePipelineDriver extends PipelineDriver {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private CompletionService<IPipeline> completionService;
	private ThreadPoolExecutor executor;
	private Stack<CallablePipeline> pipelines;
	private int workQueueCount;

	public WorkQueuePipelineDriver() {
		this(Runtime.getRuntime().availableProcessors());
	}

	public WorkQueuePipelineDriver(int workQueueCount) {
		super();
		this.workQueueCount = workQueueCount;
		pipelines = new Stack<CallablePipeline>();
		executor = (ThreadPoolExecutor)Executors.newFixedThreadPool(workQueueCount);
		completionService = new ExecutorCompletionService<IPipeline>(executor);
	}

	@Override
	public void processBatch() {
		int threadCount = 0;		
		List<IBatchItemContext> bis = getBatchItems();

		try {
			// if the number of items is less than our workqueue size then reduce the number of queues
			threadCount = Math.min(workQueueCount, bis.size());
			if (pipelines.isEmpty()) {
				// create initial workqueue pipelines
				for (int i = 0;  i < threadCount; i++) {
					CallablePipeline cp = new CallablePipeline(clonePipeline(getPipeline()));
					cp.setId("Thread: " + Integer.toString(threadCount));
					pipelines.push(cp);
				}
			}
						
			// start workqueues and process until no items are left			
			for (int i = 0; i < bis.size(); i++) {			
				IBatchItemContext item = bis.get(i);
				displayInput(item);
				// grab next available pipeline
				CallablePipeline cp = null;
				if (pipelines.isEmpty()) {
					cp = recyleFinishedPipeline();
					pipelines.push(cp);					
				}
				cp = pipelines.pop();
								
				// initialize and assign parameters to the cloned pipeline steps
				initializeClonedPipeline(cp, item);				
				cp.process(item.getRawDocument(0));

				// Set the runtime parameters
				completionService.submit(cp);
				logger.info(String.format("Pipeline thread running: %s", cp.getId()));
			}

			// wait for any remaining pipelines to finish
			for (int i = 0; i < threadCount; i++) {
				Future<IPipeline> r = completionService.take();
				IPipeline cp = r.get();
				
				// each pipeline is run independent of other inputs so we call endBatch 
				// in case there is cleanup code
				cp.endBatch();
				if (!cp.getState().equals(PipelineReturnValue.SUCCEDED)) {
					logger.error(String.format("Pipeline failure: %s", cp.getId()));
				}				
				
				// cache threaded pipelines for another call to processBatch
				pipelines.push((CallablePipeline)cp);
			}
		} catch (InstantiationException e) {
			throw new OkapiException("Error instantiating copy of pipeline.", e);
		} catch (IllegalAccessException e) {
			throw new OkapiException("Error instantiating copy of pipeline.", e);
		} catch (InterruptedException e) {
			throw new OkapiException("Error. Pipeline thread interupted", e);
		} catch (ExecutionException e) {
			throw new OkapiException(String.format("Error pipeline threw an exception. "
					+ "Cannot retrieve pipeline future from workqueue: %s", e.getCause().toString()), e);
		} finally {
			// TODO: Any cleanup to go here?
		}
	}
	
	@Override
	public void destroy() {
		super.destroy();
		for (CallablePipeline p : pipelines) {
			p.destroy();
			p = null;
		}	
		executor.shutdownNow();
		if (!executor.isShutdown()) {
			logger.error("WorkQueue Pipeline did not shutdown. Main thread still running.");
		}
	}
	
	private CallablePipeline recyleFinishedPipeline() 
			throws InterruptedException, ExecutionException, 
				InstantiationException, IllegalAccessException {
		CallablePipeline cp;
		
		// wait for a pipeline to finish so we can recycle it
		Future<IPipeline> r = completionService.take();
		cp = (CallablePipeline)r.get();
		
		// each pipeline is run independent of other inputs so we call endBatch 
		// in case there is cleanup code
		cp.endBatch();
		if (!cp.getState().equals(PipelineReturnValue.SUCCEDED)) {
			logger.error(String.format("Pipeline failure: %s", cp.getId()));
		}		
		
		return cp;
	}

	private void initializeClonedPipeline(CallablePipeline cp, IBatchItemContext item) {
		// Set the runtime parameters for the START_BATCH events
		// Especially source and target languages
		for (IPipelineStep step : cp.getSteps()) {
			PipelineDriverUtils.assignRuntimeParameters(this, step, getBatchItems().get(0), null);
		}		
		cp.startBatch();
		
		// set the parameters for current step
		for (IPipelineStep step : cp.getSteps()) {
			PipelineDriverUtils.assignRuntimeParameters(this, step, item, null);
		}
	}

	@SuppressWarnings("rawtypes")
	private IPipeline clonePipeline(IPipeline pipeline) throws InstantiationException,
			IllegalAccessException {
		IPipeline p = new Pipeline();
		for (IPipelineStep s : pipeline.getSteps()) {
			IPipelineStep cs = s.getClass().newInstance();
			// in the case of a IWorkQueueStep we must call the empty
			// Contractor, which means manual initialization
			if (s instanceof IWorkQueueStep) {				
				IWorkQueueStep wqs = (IWorkQueueStep)s;
				((IWorkQueueStep)cs).setMainStep(wqs.getMainStep());
				((IWorkQueueStep)cs).setWorkQueueCount(wqs.getWorkQueueCount());
				((IWorkQueueStep)cs).init();
			}			
			cs.setLastOutputStep(s.isLastOutputStep());
			cs.setParameters(s.getParameters());
			p.addStep(cs);
		}
		return p;
	}
}
