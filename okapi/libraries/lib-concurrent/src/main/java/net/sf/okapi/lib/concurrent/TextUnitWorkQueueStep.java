/*===========================================================================
  Copyright (C) 2009-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.concurrent;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.pipeline.ICallableStep;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.common.pipeline.IWorkQueueStep;
import net.sf.okapi.common.resource.MultiEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wrapper for a step {@link IPipelineStep} that queues up
 * TextUnits/DocumentParts and processes them concurrently.
 * <p>
 * Assumes the wrapped step is essentially stateless with regards to TextUnit
 * and DocumentPart processing. DocumentPart events are *not* processed by the step, 
 * but passed on as-is. This is normally not an issue for TextUnit centric steps.
 */
public class TextUnitWorkQueueStep implements IWorkQueueStep<SortableEvent> {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private CompletionService<SortableEvent> completionService;
	private ThreadPoolExecutor executor;
	private LinkedList<ICallableStep<SortableEvent>> steps;
	private int workQueueCount;
	private IPipelineStep step;
	private List<SortableEvent> cachedEvents;
	private int textUnitCount;

	public TextUnitWorkQueueStep() {
		steps = new LinkedList<ICallableStep<SortableEvent>>();
		cachedEvents = new LinkedList<SortableEvent>();
	}
	
	public TextUnitWorkQueueStep(IPipelineStep step, int workQueueCount)
			throws InstantiationException, IllegalAccessException {
		this();
		this.workQueueCount = workQueueCount;
		this.step = step;
		init();
	}
	
	@Override
	public void setMainStep(IPipelineStep step) {
		this.step = step;		
	}

	@Override
	public void setWorkQueueCount(int workQueueCount) {
		this.workQueueCount = workQueueCount;		
	}
	
	public void init() throws InstantiationException, IllegalAccessException {
		executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(workQueueCount);
		completionService = new ExecutorCompletionService<SortableEvent>(executor);
		
		// create initial workqueue step threads
		for (int i = 0; i < workQueueCount; i++) {
			CallableStep cs = new CallableStep(step.getClass().newInstance());
			cs.setLastOutputStep(step.isLastOutputStep());
			cs.setParameters(step.getParameters());
			steps.add(cs);
		}
	}
	
	@Override
	public IPipelineStep getMainStep() {
		return step;
	}
	
	@Override
	public LinkedList<ICallableStep<SortableEvent>> getCallableSteps() {
		return steps;
	}
	
	@Override
	public int getWorkQueueCount() {
		return workQueueCount;
	}

	@Override
	public IParameters getParameters() {
		return step.getParameters();
	}

	@Override
	public void setParameters(IParameters params) {
		step.setParameters(params);
		for (ICallableStep<SortableEvent> cs : steps) {
			cs.setParameters(params);
		}
	}

	@Override
	public String getName() {
		return String.format("%s (Threaded Work Queue)", step.getName());
	}

	@Override
	public String getDescription() {
		return String.format("%s (Threaded Work Queue)", step.getDescription());
	}

	@Override
	public String getHelpLocation() {
		return step.getHelpLocation();
	}

	@Override
	public Event handleEvent(Event event) {
		
		switch (event.getEventType()) {
		case TEXT_UNIT:
		    textUnitCount++;
		case DOCUMENT_PART:			
			cachedEvents.add(new SortableEvent(event, cachedEvents.size()));
			if (textUnitCount >= workQueueCount) {
				return processCachedTextUnits();
			} 
			return Event.NOOP_EVENT;
		// start events are sometimes used for initialization  
		// each callable step must process them (but non-threaded vis processNow() method)
		case START_BATCH:
		case START_BATCH_ITEM:
		case START_DOCUMENT:
		case START_GROUP:
		case START_SUBDOCUMENT:
		case START_SUBFILTER:
			// there shouldn't be any cached events when we see these events
			// as all END events empty the cache. Leave this assert here
			// just in case
			assert(cachedEvents.size() <= 0);
			for (ICallableStep<SortableEvent> cs : steps) {
				cs.processNow(event);
			}
			return step.handleEvent(event);				
		default:
			// handle remaining event types, mostly END* events
			// make sure the step and copies process this event
			// before passing it on, may be needed for cleanup code etc.
			if (cachedEvents.size() > 0) {
				Event me = processCachedTextUnits();
				// add the current event to the multi event				
				me.getMultiEvent().addEvent(step.handleEvent(event));
				for (ICallableStep<SortableEvent> cs : steps) {
					cs.processNow(event);
				}
				return me;
			} else {
				for (ICallableStep<SortableEvent> cs : steps) {
					cs.processNow(event);
				}
				return step.handleEvent(event);
			}		
		}
	}

	protected Event processCachedTextUnits() {
		List<Event> sortedEvents = null;
		try {
			sortedEvents = startConcurrentProcesing();
		} catch (InterruptedException e) {
			destroy();
			throw new OkapiException("Concurrent step was interrupted", e);
		} catch (ExecutionException e) {
			destroy();
			throw new OkapiException("Concurrent step threw an exception", e);
		}
		cachedEvents.clear();
		textUnitCount = 0;
		return new Event(EventType.MULTI_EVENT, new MultiEvent(sortedEvents));
	}

	protected List<Event> startConcurrentProcesing() 
			throws InterruptedException, ExecutionException {
		int i = 0;
		for (SortableEvent event : cachedEvents) {
			if (event.isTextUnit()) {
				ICallableStep<SortableEvent> cs = steps.get(i++);
				cs.handleEvent(event);
				completionService.submit(cs);
			} else {
				
			}
		}

		// wait for any remaining pipelines to finish
		for (i = 0; i < textUnitCount; i++) {
			Future<SortableEvent> r = completionService.take();
			// remove the TextUnit with the same order number
			cachedEvents.remove(r.get());
			// now replace the removed TextUnit with the one returned by the step
			cachedEvents.add(r.get());
		}

		// sort events based on the original order
		Collections.sort(cachedEvents, Collections.reverseOrder());

		// down cast to Event to keep Java happy
		List<Event> sortedEvents = new LinkedList<Event>();
		for (SortableEvent event : cachedEvents) {
			sortedEvents.add(new Event(event.getEventType(), event.getResource()));
		}

		return sortedEvents;
	}

	@Override
	public boolean isDone() {
		return step.isDone();
	}

	@Override
	public void destroy() {
		step.destroy();
		for (ICallableStep<SortableEvent> cs : steps) {
			cs.destroy();
		}
		executor.shutdownNow();
		if (!executor.isShutdown()) {
			logger.error("WorkQueue Pipeline did not shutdown. Main thread still running.");
		}
	}

	@Override
	public void cancel() {
		step.cancel();
		for (ICallableStep<SortableEvent> cs : steps) {
			cs.cancel();
		}
	}

	@Override
	public boolean isLastOutputStep() {
		return step.isLastOutputStep();
	}

	@Override
	public void setLastOutputStep(boolean isLastStep) {
		step.setLastOutputStep(isLastStep);
		for (ICallableStep<SortableEvent> cs : steps) {
			cs.setLastOutputStep(isLastStep);
		}
	}
}
