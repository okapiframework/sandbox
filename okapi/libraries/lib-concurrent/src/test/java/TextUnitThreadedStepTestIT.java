package net.sf.okapi.common.pipeline.integration;

import java.net.URISyntaxException;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.TestUtil;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import net.sf.okapi.common.pipelinedriver.IPipelineDriver;
import net.sf.okapi.common.pipelinedriver.PipelineDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.lib.concurrent.TextUnitWorkQueueStep;
import net.sf.okapi.lib.concurrent.WorkQueuePipelineDriver;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;
import net.sf.okapi.steps.segmentation.SegmentationStep;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TextUnitThreadedStepTestIT
{
	private IPipelineDriver driver;
	private IFilterConfigurationMapper fcMapper;
	private String root;
	private String[] htmlFiles;

	@Before
	public void setUp() throws Exception {
		root = TestUtil.getParentDir(this.getClass(), "/");
		htmlFiles = Util.getFilteredFiles(root + "test-classes/", ".html");
		fcMapper = new FilterConfigurationMapper();
		fcMapper.addConfigurations("net.sf.okapi.filters.html.HtmlFilter");
		driver = new PipelineDriver();
		driver.setFilterConfigurationMapper(fcMapper);
		driver.setOutputDirectory(root);
		driver.addStep(new RawDocumentToFilterEventsStep());
		driver.addStep(new TextUnitWorkQueueStep(new LongRunningStep(), 4));
		RawDocument rd = new RawDocument("<p>this is a test1</p>"
				+ "<p>this is a test2</p>"
				+ "<p>this is a test3</p>"
				+ "<p>this is a test4</p>", LocaleId.ENGLISH, LocaleId.SPANISH);
		rd.setFilterConfigId("okf_html");
		driver.addBatchItem(rd);
	}

	@After
	public void tearDown() throws Exception {
		driver.destroy();
	}

	@Test
	public void runSimpleThreadedStep() {
		driver.processBatch();
	}
	
	@Test
	public void runSimplePipelineTwice() {
		// First pass
		driver.processBatch();

		// Second pass
		driver.clearItems();
		RawDocument rd = new RawDocument("<p>this is a test1</p>"
				+ "<p>this is a test2</p>"
				+ "<p>this is a test3</p>"
				+ "<p>this is a test4</p>", LocaleId.ENGLISH, LocaleId.SPANISH);
		rd.setFilterConfigId("okf_html");
		driver.addBatchItem(rd);
		driver.processBatch();		
	}
	
	@Test
	public void runSegmentationPipeline() 
			throws URISyntaxException, InstantiationException, IllegalAccessException {
		IPipelineDriver d = createSegmentationPipeline(new PipelineDriver());
		for (String f : htmlFiles) {
			d.addBatchItem(Util.toURI(root + "test-classes/" + f), "UTF-8", "okf_html", LocaleId.ENGLISH, LocaleId.ENGLISH);
		}		
		d.processBatch();
	}
	
	@Test
	public void runSegmentationWorkQueuePipeline() 
			throws URISyntaxException, InstantiationException, IllegalAccessException {
		IPipelineDriver d = createSegmentationPipeline(new WorkQueuePipelineDriver());
		for (String f : htmlFiles) {
			d.addBatchItem(Util.toURI(root + "test-classes/" + f), "UTF-8", "okf_html", LocaleId.ENGLISH, LocaleId.ENGLISH);
		}		
		d.processBatch();
	}
	
	private IPipelineDriver createSegmentationPipeline(IPipelineDriver d) 
			throws URISyntaxException, InstantiationException, IllegalAccessException {
		d.setFilterConfigurationMapper(fcMapper);
		d.addStep(new RawDocumentToFilterEventsStep());
		
		SegmentationStep segmentationStep = new SegmentationStep();
		net.sf.okapi.steps.segmentation.Parameters sp = (net.sf.okapi.steps.segmentation.Parameters) segmentationStep.getParameters();
		sp.setSourceSrxPath(getClass().getResource("/test.srx").getPath());
		sp.setSegmentSource(true);
		d.addStep(new TextUnitWorkQueueStep(segmentationStep, 8));
		//d.addStep(segmentationStep);
		d.setOutputDirectory(root + "test-classes/");

		return d;
	}
}
