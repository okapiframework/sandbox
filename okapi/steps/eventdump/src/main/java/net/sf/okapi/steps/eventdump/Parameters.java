package net.sf.okapi.steps.eventdump;

import net.sf.okapi.common.EventType;
import net.sf.okapi.common.StringParameters;

// Not implemented for now, just some begining of the "plumbing".
// Want color palettes, and also controlling what to exclude from the dump.
public class Parameters extends StringParameters {
  private static final String COLOR_MODE = "colorMode";
  public static final String COLOR_DARK = "colorModeDark";
  public static final String COLOR_LIGHT = "colorModeLight";
  public static final String COLOR_NONE = "colorModeNone";

  public Parameters() {
    super();
    reset();
  }

  public String getColorMode() {
    return getString(COLOR_MODE);
  }
  public void setColorMode(String mode) {
    setString(COLOR_MODE, mode);
  }

  @Override
  public void reset() {
    setColorMode(COLOR_DARK);
    setDumpAllEvents(true);
  }

  // Enable / disable Event, by EventType
  public boolean getDumpEvent(EventType eventType) {
    return getBoolean(eventType.name());
  }
  public void setDumpEvent(EventType eventType, boolean enabled) {
    setBoolean(eventType.name(), enabled);
  }

  // Enable / disable all the EventTypes(s)
  public void setDumpAllEvents(boolean enabled) {
    setDumpEvent(EventType.CANCELED, enabled);
    setDumpEvent(EventType.CUSTOM, enabled);
    setDumpEvent(EventType.DOCUMENT_PART, enabled);
    setDumpEvent(EventType.END_BATCH, enabled);
    setDumpEvent(EventType.END_BATCH_ITEM, enabled);
    setDumpEvent(EventType.END_DOCUMENT, enabled);
    setDumpEvent(EventType.END_GROUP, enabled);
    setDumpEvent(EventType.END_SUBDOCUMENT, enabled);
    setDumpEvent(EventType.END_SUBFILTER, enabled);
    setDumpEvent(EventType.MULTI_EVENT, enabled);
    setDumpEvent(EventType.NO_OP, enabled);
    setDumpEvent(EventType.PIPELINE_PARAMETERS, enabled);
    setDumpEvent(EventType.RAW_DOCUMENT, enabled);
    setDumpEvent(EventType.START_BATCH, enabled);
    setDumpEvent(EventType.START_BATCH_ITEM, enabled);
    setDumpEvent(EventType.START_DOCUMENT, enabled);
    setDumpEvent(EventType.START_GROUP, enabled);
    setDumpEvent(EventType.START_SUBDOCUMENT, enabled);
    setDumpEvent(EventType.START_SUBFILTER, enabled);
    setDumpEvent(EventType.TEXT_UNIT, enabled);
  }
}
