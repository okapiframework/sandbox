package net.sf.okapi.steps.eventdump;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Objects;
import net.sf.okapi.common.resource.TextContainer;

public class OkapiTypeAdaptors {
  static class OkapiValueColorAdapter<T> implements JsonSerializer<T> {
    @Override
    public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
      return new JsonPrimitive(Colors.STRING + Objects.toString(src) + Colors.RESET);
    }
  }

  static class OkapiEnumColorAdapter<T> implements JsonSerializer<T> {
    @Override
    public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
      return new JsonPrimitive(Colors.ENUM + Objects.toString(((Enum<?>) src).name()) + Colors.RESET);
    }
  }

  // To prevent recursion and stack overflow
  static class OkapiExclusionStrategy implements ExclusionStrategy {
    @Override
    public boolean shouldSkipField(FieldAttributes f) {

      // System.out.printf("\u001b[91mshouldSkipField(%s::%s)\u001b[m%n",
      //    f.getDeclaringClass(),  f.getName());

      if ("parent".equals(f.getName())) return true;

      // We don't want to dump both TextContainer.parts and TextContainer.Segments.parts
      // because that would be a double. Segments already contains the List<TextParts>.
      if (f.getDeclaringClass() == TextContainer.class && "parts".equals(f.getName())) return true;

      // This is a for the parameters of the filter event, and it is a huge monster:
      //    net.sf.okapi.filters.abstractmarkup.AbstractMarkupParameters
      // Disabled temporarily. Might be nice to control this in the EventDumpStep Parameters.
      if ("taggedConfig".equals(f.getName())) return true;

      return false;
    }

    @Override
    public boolean shouldSkipClass(Class<?> clazz) {
      if (clazz == org.slf4j.Logger.class) return true;

      // Don't dump Java internals
      if (clazz == java.lang.Class.class) return true;
      if (clazz == java.lang.ref.Reference.class) return true;

      final String className = clazz.getName();
      if (className.startsWith("com.sun.org.")) return true;
      if (className.startsWith("sun.")) return true;

      if (className.startsWith("org.apache.xerces.")) return true;

      return false;
    }
  }

  static class ColorNamingStrategy implements FieldNamingStrategy {
    @Override
    public String translateName(Field f) {
      return Colors.LABEL + f.getName() + Colors.RESET;
    }
  }

  // Coloring for string values
  final static OkapiValueColorAdapter<?> STRING_VALUE_COLOR_ADAPTER =
      new OkapiValueColorAdapter<CharSequence>();

  // Coloring for enum values
  final static OkapiEnumColorAdapter<?> ENUM_VALUE_COLOR_ADAPTER =
      new OkapiEnumColorAdapter<CharSequence>();

  // Coloring for labels
  final static ColorNamingStrategy LABEL_COLOR_STRATEGY =
      new ColorNamingStrategy();

  // Filter out certain classes / fields to eliminate recursion
  final static OkapiExclusionStrategy EXCLUSION_STRATEGY =
      new OkapiExclusionStrategy();
}

