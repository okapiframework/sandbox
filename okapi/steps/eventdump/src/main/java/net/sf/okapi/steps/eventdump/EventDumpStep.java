package net.sf.okapi.steps.eventdump;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.pipeline.BasePipelineStep;

public class EventDumpStep extends BasePipelineStep {

  private final static Gson gson = new GsonBuilder()
      .setPrettyPrinting()
      .disableHtmlEscaping()
      .serializeSpecialFloatingPointValues()
      .setDateFormat("yyyy-MM-dd hh:mm:ss") // ISO
      .enableComplexMapKeySerialization()
      // Preventing recursion
      .addSerializationExclusionStrategy(OkapiTypeAdaptors.EXCLUSION_STRATEGY)
      // Coloring for labels
      .setFieldNamingStrategy(OkapiTypeAdaptors.LABEL_COLOR_STRATEGY)
      // Coloring for values
      .registerTypeAdapter(String.class, OkapiTypeAdaptors.STRING_VALUE_COLOR_ADAPTER)
      .registerTypeAdapter(StringBuffer.class, OkapiTypeAdaptors.STRING_VALUE_COLOR_ADAPTER)
      .registerTypeAdapter(StringBuilder.class, OkapiTypeAdaptors.STRING_VALUE_COLOR_ADAPTER)
      .registerTypeAdapter(CharSequence.class, OkapiTypeAdaptors.STRING_VALUE_COLOR_ADAPTER)
      .registerTypeAdapter(EventType.class, OkapiTypeAdaptors.ENUM_VALUE_COLOR_ADAPTER)
      .create();

  public EventDumpStep() {
    setParameters(new Parameters());
  }

  @Override
  public String getName() {
    return "EventDumpStep";
  }

  @Override
  public String getDescription() {
    return "Dumps all info about an Event to stdout.";
  }

  @Override
  public Event handleEvent(Event event) {
    Parameters param = (Parameters) getParameters();
    if (param.getDumpEvent(event.getEventType())) {
      System.out.println(Colors.COMMENT + "//===== Event: " + event + " =====" + Colors.RESET);
      System.out.println(gson.toJson(event).replaceAll("\\\\u001b", "\u001b"));
    }
    return super.handleEvent(event);
  }
}
