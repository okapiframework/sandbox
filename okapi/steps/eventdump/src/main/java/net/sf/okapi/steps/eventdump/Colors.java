package net.sf.okapi.steps.eventdump;

// ToDo: would be nice to have dark / light "palettes"
public class Colors {
  final static String STRING = "\u001b[96m";
  final static String ENUM = "\u001b[97m";
  final static String COMMENT = "\u001b[32m";
  final static String LABEL = "\u001b[93m";
  final static String RESET = "\u001b[m";
}
