package net.sf.okapi.steps.eventdump;

import java.io.File;
import java.util.Arrays;

import net.sf.okapi.common.filters.DefaultFilters;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import net.sf.okapi.common.pipelinedriver.BatchItemContext;
import net.sf.okapi.common.pipelinedriver.IPipelineDriver;
import net.sf.okapi.common.pipelinedriver.PipelineDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;
import net.sf.okapi.steps.eventdump.EventDumpStep;
import net.sf.okapi.steps.segmentation.Parameters.SegmStrategy;
import net.sf.okapi.steps.segmentation.SegmentationStep;
import net.sf.okapi.steps.wordcount.WordCountStep;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
@SuppressWarnings("static-method")
public class EventDumpStepTest {
  static final LocaleId SOURCE_LOCALE = LocaleId.ENGLISH;
  static final LocaleId TARGET_LOCALE = LocaleId.FRENCH;
  static final String DEFAULT_CHARSET = "UTF-8";
  static final String INPUT_DIR = "dataIn";
  static final String OUTPUT_DIR = "dataOut";
  static final String ROOT_DIR = EventDumpStepTest.class.getResource("/").getFile();
  static final String SRX_FILE = new File(ROOT_DIR, "config/defaultSegmentation.srx").toString();

  private static IFilterConfigurationMapper fcMapper = new FilterConfigurationMapper();
  static {
    DefaultFilters.setMappings(fcMapper, true, true);
  }

  public static void main(String[] args) {
    new EventDumpStepTest().test();
  }

  @Test
  public void test() {
    final IPipelineDriver driver = createDriver(OUTPUT_DIR);

    driver.addStep(new RawDocumentToFilterEventsStep());

    final SegmentationStep segStep = new SegmentationStep();
    final net.sf.okapi.steps.segmentation.Parameters params =
        (net.sf.okapi.steps.segmentation.Parameters) segStep.getParameters();
    params.setSourceSrxPath(SRX_FILE);
    params.setTargetSrxPath(SRX_FILE);
    params.setSegmentationStrategy(SegmStrategy.OVERWRITE_EXISTING);
    params.setSegmentTarget(true);
    params.setSegmentSource(true);
    segStep.setParameters(params);
    segStep.setSourceLocale(SOURCE_LOCALE);
    segStep.setTargetLocales(Arrays.asList(TARGET_LOCALE));
    driver.addStep(segStep);

    final WordCountStep wcStep = new WordCountStep();
    wcStep.setSourceLocale(SOURCE_LOCALE);
    wcStep.setTargetLocale(TARGET_LOCALE);
    driver.addStep(wcStep);

    final EventDumpStep dump = new EventDumpStep();
//    Parameters param = (Parameters) dump.getParameters();
//    param.setColorMode(param.COLOR_DARK);
    driver.addStep(dump);

    final File inputDir = new File(ROOT_DIR, INPUT_DIR);
    for (File inputFile : inputDir.listFiles()) {
      addFileToDriver(driver, inputFile);
    }

    // Run the pipeline
    driver.processBatch();

    System.out.println("DONE");
  }

  static void addFileToDriver(IPipelineDriver driver, File inputFile) {
    if (!inputFile.isFile()) {
      return;
    }

    String path = inputFile.getAbsolutePath();
    // inputDir/foo.html => outputDir/foo.html.xlf
    File outputFile = new File(OUTPUT_DIR, Util.getFilename(path, true) + ".xlf");

    // Find out the default import filter for the file, based on extension
    String ext = Util.getExtension(path);
    FilterConfiguration cfg = fcMapper.getDefaultConfigurationFromExtension(ext);

    if (cfg != null) {
    	// Create the RawDocument
//    	cfg.configId = "okf_html";
    	System.out.printf("Adding %-15s : %s%n", cfg.configId, inputFile);
    	@SuppressWarnings("resource")
    	RawDocument rawDoc = new RawDocument(inputFile.toURI(), DEFAULT_CHARSET,
    		SOURCE_LOCALE, TARGET_LOCALE, cfg.configId);

    	// Create the batch item to process and add it to the driver
    	BatchItemContext item = new BatchItemContext(rawDoc, outputFile.toURI(), DEFAULT_CHARSET);
    	driver.addBatchItem(item);
    }
  }

  static IPipelineDriver createDriver(String root) {
    // Create the pipeline driver
    IPipelineDriver driver = new PipelineDriver();

    // Set the filter configuration map to use with the driver
    driver.setFilterConfigurationMapper(fcMapper);

    // Set the root folder for the driver's context
    driver.setRootDirectories(root, root);
    driver.setOutputDirectory(root);

    return driver;
  }
}
