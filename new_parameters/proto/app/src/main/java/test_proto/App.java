package test_proto;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import com.google.protobuf.TextFormat;
import com.google.protobuf.util.JsonFormat;

import net.sf.okapi.filters.xliff.Parameters;

public class App {

    static void saveProto(String baseFileName, Parameters param) throws IOException {
        Path outDir = Paths.get("..", "out");
        Files.createDirectories(outDir);

        // Save in binary form
        try (OutputStream out = Files.newOutputStream(outDir.resolve(baseFileName + "_binary.proto.bin"))) {
            param.writeTo(out);
        }

        String toWrite = param.toString();
        Files.write(
            outDir.resolve(baseFileName + "_tostring.proto.txt"),
            toWrite.getBytes(StandardCharsets.UTF_8));

        // This gives us more control than toString. For example how to treat non-ASCII
        toWrite = TextFormat.printer().escapingNonAscii(false).printToString(param);
        Files.write(
            outDir.resolve(baseFileName + "_printer.proto.txt"),
            toWrite.getBytes(StandardCharsets.UTF_8));

        toWrite = JsonFormat.printer().print(param);
        Files.write(
            outDir.resolve(baseFileName + "_printer.proto.json"),
            toWrite.getBytes(StandardCharsets.UTF_8));

        toWrite = JsonFormat.printer().includingDefaultValueFields().print(param);
        Files.write(
            outDir.resolve(baseFileName + "_printer_all.proto.json"),
            toWrite.getBytes(StandardCharsets.UTF_8));
    }

    static Parameters defaultProto() {
        return Parameters.newBuilder().build();
    }

    static Parameters modifiedProto() {
        return Parameters.newBuilder()
            .setSomeString("New value. With * \u65E5\u672C and \n and Ελληνικά")
            .setSomeDouble(0.5772)
            .setSomeInteger(13)
            .setSomeBoolean(true)
            .setSegmentationType(Parameters.SegmentationType.NOTSEGMENTED)
            .setSourceLocale("en")
            .addAllTargetLocales(List.of("fr", "it", "de", "es")) // all at once
            .addTargetLocales("es-419") // add extra
            .putAllSomeMap(Map.of("1", "one", "2", "two", "3", "three"))
            .putSomeMap("4", "four")
            .build()
            ;
    }
}
