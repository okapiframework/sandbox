// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: main/proto/TextUnit.proto

package net.sf.okapi.proto.events;

/**
 * Protobuf type {@code IResource}
 */
public  final class IResource extends
    com.google.protobuf.GeneratedMessage implements
    // @@protoc_insertion_point(message_implements:IResource)
    IResourceOrBuilder {
  // Use IResource.newBuilder() to construct.
  private IResource(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
    super(builder);
  }
  private IResource() {
    id_ = "";
    annotations_ = com.google.protobuf.LazyStringArrayList.EMPTY;
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private IResource(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 10: {
            java.lang.String s = input.readStringRequireUtf8();

            id_ = s;
            break;
          }
          case 18: {
            net.sf.okapi.proto.events.ISkeleton.Builder subBuilder = null;
            if (skeleton_ != null) {
              subBuilder = skeleton_.toBuilder();
            }
            skeleton_ = input.readMessage(net.sf.okapi.proto.events.ISkeleton.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(skeleton_);
              skeleton_ = subBuilder.buildPartial();
            }

            break;
          }
          case 26: {
            java.lang.String s = input.readStringRequireUtf8();
            if (!((mutable_bitField0_ & 0x00000004) == 0x00000004)) {
              annotations_ = new com.google.protobuf.LazyStringArrayList();
              mutable_bitField0_ |= 0x00000004;
            }
            annotations_.add(s);
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      if (((mutable_bitField0_ & 0x00000004) == 0x00000004)) {
        annotations_ = annotations_.getUnmodifiableView();
      }
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_IResource_descriptor;
  }

  protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_IResource_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            net.sf.okapi.proto.events.IResource.class, net.sf.okapi.proto.events.IResource.Builder.class);
  }

  private int bitField0_;
  public static final int ID_FIELD_NUMBER = 1;
  private volatile java.lang.Object id_;
  /**
   * <code>optional string id = 1;</code>
   */
  public java.lang.String getId() {
    java.lang.Object ref = id_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      id_ = s;
      return s;
    }
  }
  /**
   * <code>optional string id = 1;</code>
   */
  public com.google.protobuf.ByteString
      getIdBytes() {
    java.lang.Object ref = id_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      id_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int SKELETON_FIELD_NUMBER = 2;
  private net.sf.okapi.proto.events.ISkeleton skeleton_;
  /**
   * <code>optional .ISkeleton skeleton = 2;</code>
   */
  public boolean hasSkeleton() {
    return skeleton_ != null;
  }
  /**
   * <code>optional .ISkeleton skeleton = 2;</code>
   */
  public net.sf.okapi.proto.events.ISkeleton getSkeleton() {
    return skeleton_ == null ? net.sf.okapi.proto.events.ISkeleton.getDefaultInstance() : skeleton_;
  }
  /**
   * <code>optional .ISkeleton skeleton = 2;</code>
   */
  public net.sf.okapi.proto.events.ISkeletonOrBuilder getSkeletonOrBuilder() {
    return getSkeleton();
  }

  public static final int ANNOTATIONS_FIELD_NUMBER = 3;
  private com.google.protobuf.LazyStringList annotations_;
  /**
   * <code>repeated string annotations = 3;</code>
   */
  public com.google.protobuf.ProtocolStringList
      getAnnotationsList() {
    return annotations_;
  }
  /**
   * <code>repeated string annotations = 3;</code>
   */
  public int getAnnotationsCount() {
    return annotations_.size();
  }
  /**
   * <code>repeated string annotations = 3;</code>
   */
  public java.lang.String getAnnotations(int index) {
    return annotations_.get(index);
  }
  /**
   * <code>repeated string annotations = 3;</code>
   */
  public com.google.protobuf.ByteString
      getAnnotationsBytes(int index) {
    return annotations_.getByteString(index);
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (!getIdBytes().isEmpty()) {
      com.google.protobuf.GeneratedMessage.writeString(output, 1, id_);
    }
    if (skeleton_ != null) {
      output.writeMessage(2, getSkeleton());
    }
    for (int i = 0; i < annotations_.size(); i++) {
      com.google.protobuf.GeneratedMessage.writeString(output, 3, annotations_.getRaw(i));
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (!getIdBytes().isEmpty()) {
      size += com.google.protobuf.GeneratedMessage.computeStringSize(1, id_);
    }
    if (skeleton_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getSkeleton());
    }
    {
      int dataSize = 0;
      for (int i = 0; i < annotations_.size(); i++) {
        dataSize += computeStringSizeNoTag(annotations_.getRaw(i));
      }
      size += dataSize;
      size += 1 * getAnnotationsList().size();
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  public static net.sf.okapi.proto.events.IResource parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static net.sf.okapi.proto.events.IResource parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.IResource parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static net.sf.okapi.proto.events.IResource parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.IResource parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static net.sf.okapi.proto.events.IResource parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.IResource parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static net.sf.okapi.proto.events.IResource parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.IResource parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static net.sf.okapi.proto.events.IResource parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(net.sf.okapi.proto.events.IResource prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessage.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code IResource}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessage.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:IResource)
      net.sf.okapi.proto.events.IResourceOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_IResource_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_IResource_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              net.sf.okapi.proto.events.IResource.class, net.sf.okapi.proto.events.IResource.Builder.class);
    }

    // Construct using net.sf.okapi.proto.events.IResource.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      id_ = "";

      if (skeletonBuilder_ == null) {
        skeleton_ = null;
      } else {
        skeleton_ = null;
        skeletonBuilder_ = null;
      }
      annotations_ = com.google.protobuf.LazyStringArrayList.EMPTY;
      bitField0_ = (bitField0_ & ~0x00000004);
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_IResource_descriptor;
    }

    public net.sf.okapi.proto.events.IResource getDefaultInstanceForType() {
      return net.sf.okapi.proto.events.IResource.getDefaultInstance();
    }

    public net.sf.okapi.proto.events.IResource build() {
      net.sf.okapi.proto.events.IResource result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public net.sf.okapi.proto.events.IResource buildPartial() {
      net.sf.okapi.proto.events.IResource result = new net.sf.okapi.proto.events.IResource(this);
      int from_bitField0_ = bitField0_;
      int to_bitField0_ = 0;
      result.id_ = id_;
      if (skeletonBuilder_ == null) {
        result.skeleton_ = skeleton_;
      } else {
        result.skeleton_ = skeletonBuilder_.build();
      }
      if (((bitField0_ & 0x00000004) == 0x00000004)) {
        annotations_ = annotations_.getUnmodifiableView();
        bitField0_ = (bitField0_ & ~0x00000004);
      }
      result.annotations_ = annotations_;
      result.bitField0_ = to_bitField0_;
      onBuilt();
      return result;
    }

    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof net.sf.okapi.proto.events.IResource) {
        return mergeFrom((net.sf.okapi.proto.events.IResource)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(net.sf.okapi.proto.events.IResource other) {
      if (other == net.sf.okapi.proto.events.IResource.getDefaultInstance()) return this;
      if (!other.getId().isEmpty()) {
        id_ = other.id_;
        onChanged();
      }
      if (other.hasSkeleton()) {
        mergeSkeleton(other.getSkeleton());
      }
      if (!other.annotations_.isEmpty()) {
        if (annotations_.isEmpty()) {
          annotations_ = other.annotations_;
          bitField0_ = (bitField0_ & ~0x00000004);
        } else {
          ensureAnnotationsIsMutable();
          annotations_.addAll(other.annotations_);
        }
        onChanged();
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      net.sf.okapi.proto.events.IResource parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (net.sf.okapi.proto.events.IResource) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int bitField0_;

    private java.lang.Object id_ = "";
    /**
     * <code>optional string id = 1;</code>
     */
    public java.lang.String getId() {
      java.lang.Object ref = id_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        id_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>optional string id = 1;</code>
     */
    public com.google.protobuf.ByteString
        getIdBytes() {
      java.lang.Object ref = id_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        id_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>optional string id = 1;</code>
     */
    public Builder setId(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      id_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional string id = 1;</code>
     */
    public Builder clearId() {
      
      id_ = getDefaultInstance().getId();
      onChanged();
      return this;
    }
    /**
     * <code>optional string id = 1;</code>
     */
    public Builder setIdBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      id_ = value;
      onChanged();
      return this;
    }

    private net.sf.okapi.proto.events.ISkeleton skeleton_ = null;
    private com.google.protobuf.SingleFieldBuilder<
        net.sf.okapi.proto.events.ISkeleton, net.sf.okapi.proto.events.ISkeleton.Builder, net.sf.okapi.proto.events.ISkeletonOrBuilder> skeletonBuilder_;
    /**
     * <code>optional .ISkeleton skeleton = 2;</code>
     */
    public boolean hasSkeleton() {
      return skeletonBuilder_ != null || skeleton_ != null;
    }
    /**
     * <code>optional .ISkeleton skeleton = 2;</code>
     */
    public net.sf.okapi.proto.events.ISkeleton getSkeleton() {
      if (skeletonBuilder_ == null) {
        return skeleton_ == null ? net.sf.okapi.proto.events.ISkeleton.getDefaultInstance() : skeleton_;
      } else {
        return skeletonBuilder_.getMessage();
      }
    }
    /**
     * <code>optional .ISkeleton skeleton = 2;</code>
     */
    public Builder setSkeleton(net.sf.okapi.proto.events.ISkeleton value) {
      if (skeletonBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        skeleton_ = value;
        onChanged();
      } else {
        skeletonBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>optional .ISkeleton skeleton = 2;</code>
     */
    public Builder setSkeleton(
        net.sf.okapi.proto.events.ISkeleton.Builder builderForValue) {
      if (skeletonBuilder_ == null) {
        skeleton_ = builderForValue.build();
        onChanged();
      } else {
        skeletonBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>optional .ISkeleton skeleton = 2;</code>
     */
    public Builder mergeSkeleton(net.sf.okapi.proto.events.ISkeleton value) {
      if (skeletonBuilder_ == null) {
        if (skeleton_ != null) {
          skeleton_ =
            net.sf.okapi.proto.events.ISkeleton.newBuilder(skeleton_).mergeFrom(value).buildPartial();
        } else {
          skeleton_ = value;
        }
        onChanged();
      } else {
        skeletonBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>optional .ISkeleton skeleton = 2;</code>
     */
    public Builder clearSkeleton() {
      if (skeletonBuilder_ == null) {
        skeleton_ = null;
        onChanged();
      } else {
        skeleton_ = null;
        skeletonBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>optional .ISkeleton skeleton = 2;</code>
     */
    public net.sf.okapi.proto.events.ISkeleton.Builder getSkeletonBuilder() {
      
      onChanged();
      return getSkeletonFieldBuilder().getBuilder();
    }
    /**
     * <code>optional .ISkeleton skeleton = 2;</code>
     */
    public net.sf.okapi.proto.events.ISkeletonOrBuilder getSkeletonOrBuilder() {
      if (skeletonBuilder_ != null) {
        return skeletonBuilder_.getMessageOrBuilder();
      } else {
        return skeleton_ == null ?
            net.sf.okapi.proto.events.ISkeleton.getDefaultInstance() : skeleton_;
      }
    }
    /**
     * <code>optional .ISkeleton skeleton = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilder<
        net.sf.okapi.proto.events.ISkeleton, net.sf.okapi.proto.events.ISkeleton.Builder, net.sf.okapi.proto.events.ISkeletonOrBuilder> 
        getSkeletonFieldBuilder() {
      if (skeletonBuilder_ == null) {
        skeletonBuilder_ = new com.google.protobuf.SingleFieldBuilder<
            net.sf.okapi.proto.events.ISkeleton, net.sf.okapi.proto.events.ISkeleton.Builder, net.sf.okapi.proto.events.ISkeletonOrBuilder>(
                getSkeleton(),
                getParentForChildren(),
                isClean());
        skeleton_ = null;
      }
      return skeletonBuilder_;
    }

    private com.google.protobuf.LazyStringList annotations_ = com.google.protobuf.LazyStringArrayList.EMPTY;
    private void ensureAnnotationsIsMutable() {
      if (!((bitField0_ & 0x00000004) == 0x00000004)) {
        annotations_ = new com.google.protobuf.LazyStringArrayList(annotations_);
        bitField0_ |= 0x00000004;
       }
    }
    /**
     * <code>repeated string annotations = 3;</code>
     */
    public com.google.protobuf.ProtocolStringList
        getAnnotationsList() {
      return annotations_.getUnmodifiableView();
    }
    /**
     * <code>repeated string annotations = 3;</code>
     */
    public int getAnnotationsCount() {
      return annotations_.size();
    }
    /**
     * <code>repeated string annotations = 3;</code>
     */
    public java.lang.String getAnnotations(int index) {
      return annotations_.get(index);
    }
    /**
     * <code>repeated string annotations = 3;</code>
     */
    public com.google.protobuf.ByteString
        getAnnotationsBytes(int index) {
      return annotations_.getByteString(index);
    }
    /**
     * <code>repeated string annotations = 3;</code>
     */
    public Builder setAnnotations(
        int index, java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  ensureAnnotationsIsMutable();
      annotations_.set(index, value);
      onChanged();
      return this;
    }
    /**
     * <code>repeated string annotations = 3;</code>
     */
    public Builder addAnnotations(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  ensureAnnotationsIsMutable();
      annotations_.add(value);
      onChanged();
      return this;
    }
    /**
     * <code>repeated string annotations = 3;</code>
     */
    public Builder addAllAnnotations(
        java.lang.Iterable<java.lang.String> values) {
      ensureAnnotationsIsMutable();
      com.google.protobuf.AbstractMessageLite.Builder.addAll(
          values, annotations_);
      onChanged();
      return this;
    }
    /**
     * <code>repeated string annotations = 3;</code>
     */
    public Builder clearAnnotations() {
      annotations_ = com.google.protobuf.LazyStringArrayList.EMPTY;
      bitField0_ = (bitField0_ & ~0x00000004);
      onChanged();
      return this;
    }
    /**
     * <code>repeated string annotations = 3;</code>
     */
    public Builder addAnnotationsBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      ensureAnnotationsIsMutable();
      annotations_.add(value);
      onChanged();
      return this;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:IResource)
  }

  // @@protoc_insertion_point(class_scope:IResource)
  private static final net.sf.okapi.proto.events.IResource DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new net.sf.okapi.proto.events.IResource();
  }

  public static net.sf.okapi.proto.events.IResource getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<IResource>
      PARSER = new com.google.protobuf.AbstractParser<IResource>() {
    public IResource parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new IResource(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<IResource> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<IResource> getParserForType() {
    return PARSER;
  }

  public net.sf.okapi.proto.events.IResource getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

