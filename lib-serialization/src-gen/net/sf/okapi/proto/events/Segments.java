// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: main/proto/TextUnit.proto

package net.sf.okapi.proto.events;

/**
 * Protobuf type {@code Segments}
 */
public  final class Segments extends
    com.google.protobuf.GeneratedMessage implements
    // @@protoc_insertion_point(message_implements:Segments)
    SegmentsOrBuilder {
  // Use Segments.newBuilder() to construct.
  private Segments(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
    super(builder);
  }
  private Segments() {
    alignmentStatus_ = 0;
    parts_ = java.util.Collections.emptyList();
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private Segments(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 8: {
            int rawValue = input.readEnum();

            alignmentStatus_ = rawValue;
            break;
          }
          case 18: {
            net.sf.okapi.proto.events.TextContainer.Builder subBuilder = null;
            if (parent_ != null) {
              subBuilder = parent_.toBuilder();
            }
            parent_ = input.readMessage(net.sf.okapi.proto.events.TextContainer.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(parent_);
              parent_ = subBuilder.buildPartial();
            }

            break;
          }
          case 26: {
            if (!((mutable_bitField0_ & 0x00000004) == 0x00000004)) {
              parts_ = new java.util.ArrayList<net.sf.okapi.proto.events.TextPart>();
              mutable_bitField0_ |= 0x00000004;
            }
            parts_.add(input.readMessage(net.sf.okapi.proto.events.TextPart.parser(), extensionRegistry));
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      if (((mutable_bitField0_ & 0x00000004) == 0x00000004)) {
        parts_ = java.util.Collections.unmodifiableList(parts_);
      }
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_Segments_descriptor;
  }

  protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_Segments_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            net.sf.okapi.proto.events.Segments.class, net.sf.okapi.proto.events.Segments.Builder.class);
  }

  /**
   * Protobuf enum {@code Segments.AlignmentStatus}
   */
  public enum AlignmentStatus
      implements com.google.protobuf.ProtocolMessageEnum {
    /**
     * <code>ALIGNED = 0;</code>
     */
    ALIGNED(0),
    /**
     * <code>NOT_ALIGNED = 1;</code>
     */
    NOT_ALIGNED(1),
    UNRECOGNIZED(-1),
    ;

    /**
     * <code>ALIGNED = 0;</code>
     */
    public static final int ALIGNED_VALUE = 0;
    /**
     * <code>NOT_ALIGNED = 1;</code>
     */
    public static final int NOT_ALIGNED_VALUE = 1;


    public final int getNumber() {
      if (this == UNRECOGNIZED) {
        throw new java.lang.IllegalArgumentException(
            "Can't get the number of an unknown enum value.");
      }
      return value;
    }

    /**
     * @deprecated Use {@link #forNumber(int)} instead.
     */
    @java.lang.Deprecated
    public static AlignmentStatus valueOf(int value) {
      return forNumber(value);
    }

    public static AlignmentStatus forNumber(int value) {
      switch (value) {
        case 0: return ALIGNED;
        case 1: return NOT_ALIGNED;
        default: return null;
      }
    }

    public static com.google.protobuf.Internal.EnumLiteMap<AlignmentStatus>
        internalGetValueMap() {
      return internalValueMap;
    }
    private static final com.google.protobuf.Internal.EnumLiteMap<
        AlignmentStatus> internalValueMap =
          new com.google.protobuf.Internal.EnumLiteMap<AlignmentStatus>() {
            public AlignmentStatus findValueByNumber(int number) {
              return AlignmentStatus.forNumber(number);
            }
          };

    public final com.google.protobuf.Descriptors.EnumValueDescriptor
        getValueDescriptor() {
      return getDescriptor().getValues().get(ordinal());
    }
    public final com.google.protobuf.Descriptors.EnumDescriptor
        getDescriptorForType() {
      return getDescriptor();
    }
    public static final com.google.protobuf.Descriptors.EnumDescriptor
        getDescriptor() {
      return net.sf.okapi.proto.events.Segments.getDescriptor().getEnumTypes().get(0);
    }

    private static final AlignmentStatus[] VALUES = values();

    public static AlignmentStatus valueOf(
        com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
      if (desc.getType() != getDescriptor()) {
        throw new java.lang.IllegalArgumentException(
          "EnumValueDescriptor is not for this type.");
      }
      if (desc.getIndex() == -1) {
        return UNRECOGNIZED;
      }
      return VALUES[desc.getIndex()];
    }

    private final int value;

    private AlignmentStatus(int value) {
      this.value = value;
    }

    // @@protoc_insertion_point(enum_scope:Segments.AlignmentStatus)
  }

  private int bitField0_;
  public static final int ALIGNMENTSTATUS_FIELD_NUMBER = 1;
  private int alignmentStatus_;
  /**
   * <code>optional .Segments.AlignmentStatus alignmentStatus = 1;</code>
   */
  public int getAlignmentStatusValue() {
    return alignmentStatus_;
  }
  /**
   * <code>optional .Segments.AlignmentStatus alignmentStatus = 1;</code>
   */
  public net.sf.okapi.proto.events.Segments.AlignmentStatus getAlignmentStatus() {
    net.sf.okapi.proto.events.Segments.AlignmentStatus result = net.sf.okapi.proto.events.Segments.AlignmentStatus.forNumber(alignmentStatus_);
    return result == null ? net.sf.okapi.proto.events.Segments.AlignmentStatus.UNRECOGNIZED : result;
  }

  public static final int PARENT_FIELD_NUMBER = 2;
  private net.sf.okapi.proto.events.TextContainer parent_;
  /**
   * <code>optional .TextContainer parent = 2;</code>
   */
  public boolean hasParent() {
    return parent_ != null;
  }
  /**
   * <code>optional .TextContainer parent = 2;</code>
   */
  public net.sf.okapi.proto.events.TextContainer getParent() {
    return parent_ == null ? net.sf.okapi.proto.events.TextContainer.getDefaultInstance() : parent_;
  }
  /**
   * <code>optional .TextContainer parent = 2;</code>
   */
  public net.sf.okapi.proto.events.TextContainerOrBuilder getParentOrBuilder() {
    return getParent();
  }

  public static final int PARTS_FIELD_NUMBER = 3;
  private java.util.List<net.sf.okapi.proto.events.TextPart> parts_;
  /**
   * <code>repeated .TextPart parts = 3;</code>
   */
  public java.util.List<net.sf.okapi.proto.events.TextPart> getPartsList() {
    return parts_;
  }
  /**
   * <code>repeated .TextPart parts = 3;</code>
   */
  public java.util.List<? extends net.sf.okapi.proto.events.TextPartOrBuilder> 
      getPartsOrBuilderList() {
    return parts_;
  }
  /**
   * <code>repeated .TextPart parts = 3;</code>
   */
  public int getPartsCount() {
    return parts_.size();
  }
  /**
   * <code>repeated .TextPart parts = 3;</code>
   */
  public net.sf.okapi.proto.events.TextPart getParts(int index) {
    return parts_.get(index);
  }
  /**
   * <code>repeated .TextPart parts = 3;</code>
   */
  public net.sf.okapi.proto.events.TextPartOrBuilder getPartsOrBuilder(
      int index) {
    return parts_.get(index);
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (alignmentStatus_ != net.sf.okapi.proto.events.Segments.AlignmentStatus.ALIGNED.getNumber()) {
      output.writeEnum(1, alignmentStatus_);
    }
    if (parent_ != null) {
      output.writeMessage(2, getParent());
    }
    for (int i = 0; i < parts_.size(); i++) {
      output.writeMessage(3, parts_.get(i));
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (alignmentStatus_ != net.sf.okapi.proto.events.Segments.AlignmentStatus.ALIGNED.getNumber()) {
      size += com.google.protobuf.CodedOutputStream
        .computeEnumSize(1, alignmentStatus_);
    }
    if (parent_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getParent());
    }
    for (int i = 0; i < parts_.size(); i++) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(3, parts_.get(i));
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  public static net.sf.okapi.proto.events.Segments parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static net.sf.okapi.proto.events.Segments parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.Segments parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static net.sf.okapi.proto.events.Segments parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.Segments parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static net.sf.okapi.proto.events.Segments parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.Segments parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static net.sf.okapi.proto.events.Segments parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.Segments parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static net.sf.okapi.proto.events.Segments parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(net.sf.okapi.proto.events.Segments prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessage.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code Segments}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessage.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:Segments)
      net.sf.okapi.proto.events.SegmentsOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_Segments_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_Segments_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              net.sf.okapi.proto.events.Segments.class, net.sf.okapi.proto.events.Segments.Builder.class);
    }

    // Construct using net.sf.okapi.proto.events.Segments.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
        getPartsFieldBuilder();
      }
    }
    public Builder clear() {
      super.clear();
      alignmentStatus_ = 0;

      if (parentBuilder_ == null) {
        parent_ = null;
      } else {
        parent_ = null;
        parentBuilder_ = null;
      }
      if (partsBuilder_ == null) {
        parts_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000004);
      } else {
        partsBuilder_.clear();
      }
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_Segments_descriptor;
    }

    public net.sf.okapi.proto.events.Segments getDefaultInstanceForType() {
      return net.sf.okapi.proto.events.Segments.getDefaultInstance();
    }

    public net.sf.okapi.proto.events.Segments build() {
      net.sf.okapi.proto.events.Segments result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public net.sf.okapi.proto.events.Segments buildPartial() {
      net.sf.okapi.proto.events.Segments result = new net.sf.okapi.proto.events.Segments(this);
      int from_bitField0_ = bitField0_;
      int to_bitField0_ = 0;
      result.alignmentStatus_ = alignmentStatus_;
      if (parentBuilder_ == null) {
        result.parent_ = parent_;
      } else {
        result.parent_ = parentBuilder_.build();
      }
      if (partsBuilder_ == null) {
        if (((bitField0_ & 0x00000004) == 0x00000004)) {
          parts_ = java.util.Collections.unmodifiableList(parts_);
          bitField0_ = (bitField0_ & ~0x00000004);
        }
        result.parts_ = parts_;
      } else {
        result.parts_ = partsBuilder_.build();
      }
      result.bitField0_ = to_bitField0_;
      onBuilt();
      return result;
    }

    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof net.sf.okapi.proto.events.Segments) {
        return mergeFrom((net.sf.okapi.proto.events.Segments)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(net.sf.okapi.proto.events.Segments other) {
      if (other == net.sf.okapi.proto.events.Segments.getDefaultInstance()) return this;
      if (other.alignmentStatus_ != 0) {
        setAlignmentStatusValue(other.getAlignmentStatusValue());
      }
      if (other.hasParent()) {
        mergeParent(other.getParent());
      }
      if (partsBuilder_ == null) {
        if (!other.parts_.isEmpty()) {
          if (parts_.isEmpty()) {
            parts_ = other.parts_;
            bitField0_ = (bitField0_ & ~0x00000004);
          } else {
            ensurePartsIsMutable();
            parts_.addAll(other.parts_);
          }
          onChanged();
        }
      } else {
        if (!other.parts_.isEmpty()) {
          if (partsBuilder_.isEmpty()) {
            partsBuilder_.dispose();
            partsBuilder_ = null;
            parts_ = other.parts_;
            bitField0_ = (bitField0_ & ~0x00000004);
            partsBuilder_ = 
              com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders ?
                 getPartsFieldBuilder() : null;
          } else {
            partsBuilder_.addAllMessages(other.parts_);
          }
        }
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      net.sf.okapi.proto.events.Segments parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (net.sf.okapi.proto.events.Segments) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int bitField0_;

    private int alignmentStatus_ = 0;
    /**
     * <code>optional .Segments.AlignmentStatus alignmentStatus = 1;</code>
     */
    public int getAlignmentStatusValue() {
      return alignmentStatus_;
    }
    /**
     * <code>optional .Segments.AlignmentStatus alignmentStatus = 1;</code>
     */
    public Builder setAlignmentStatusValue(int value) {
      alignmentStatus_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional .Segments.AlignmentStatus alignmentStatus = 1;</code>
     */
    public net.sf.okapi.proto.events.Segments.AlignmentStatus getAlignmentStatus() {
      net.sf.okapi.proto.events.Segments.AlignmentStatus result = net.sf.okapi.proto.events.Segments.AlignmentStatus.forNumber(alignmentStatus_);
      return result == null ? net.sf.okapi.proto.events.Segments.AlignmentStatus.UNRECOGNIZED : result;
    }
    /**
     * <code>optional .Segments.AlignmentStatus alignmentStatus = 1;</code>
     */
    public Builder setAlignmentStatus(net.sf.okapi.proto.events.Segments.AlignmentStatus value) {
      if (value == null) {
        throw new NullPointerException();
      }
      
      alignmentStatus_ = value.getNumber();
      onChanged();
      return this;
    }
    /**
     * <code>optional .Segments.AlignmentStatus alignmentStatus = 1;</code>
     */
    public Builder clearAlignmentStatus() {
      
      alignmentStatus_ = 0;
      onChanged();
      return this;
    }

    private net.sf.okapi.proto.events.TextContainer parent_ = null;
    private com.google.protobuf.SingleFieldBuilder<
        net.sf.okapi.proto.events.TextContainer, net.sf.okapi.proto.events.TextContainer.Builder, net.sf.okapi.proto.events.TextContainerOrBuilder> parentBuilder_;
    /**
     * <code>optional .TextContainer parent = 2;</code>
     */
    public boolean hasParent() {
      return parentBuilder_ != null || parent_ != null;
    }
    /**
     * <code>optional .TextContainer parent = 2;</code>
     */
    public net.sf.okapi.proto.events.TextContainer getParent() {
      if (parentBuilder_ == null) {
        return parent_ == null ? net.sf.okapi.proto.events.TextContainer.getDefaultInstance() : parent_;
      } else {
        return parentBuilder_.getMessage();
      }
    }
    /**
     * <code>optional .TextContainer parent = 2;</code>
     */
    public Builder setParent(net.sf.okapi.proto.events.TextContainer value) {
      if (parentBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        parent_ = value;
        onChanged();
      } else {
        parentBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>optional .TextContainer parent = 2;</code>
     */
    public Builder setParent(
        net.sf.okapi.proto.events.TextContainer.Builder builderForValue) {
      if (parentBuilder_ == null) {
        parent_ = builderForValue.build();
        onChanged();
      } else {
        parentBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>optional .TextContainer parent = 2;</code>
     */
    public Builder mergeParent(net.sf.okapi.proto.events.TextContainer value) {
      if (parentBuilder_ == null) {
        if (parent_ != null) {
          parent_ =
            net.sf.okapi.proto.events.TextContainer.newBuilder(parent_).mergeFrom(value).buildPartial();
        } else {
          parent_ = value;
        }
        onChanged();
      } else {
        parentBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>optional .TextContainer parent = 2;</code>
     */
    public Builder clearParent() {
      if (parentBuilder_ == null) {
        parent_ = null;
        onChanged();
      } else {
        parent_ = null;
        parentBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>optional .TextContainer parent = 2;</code>
     */
    public net.sf.okapi.proto.events.TextContainer.Builder getParentBuilder() {
      
      onChanged();
      return getParentFieldBuilder().getBuilder();
    }
    /**
     * <code>optional .TextContainer parent = 2;</code>
     */
    public net.sf.okapi.proto.events.TextContainerOrBuilder getParentOrBuilder() {
      if (parentBuilder_ != null) {
        return parentBuilder_.getMessageOrBuilder();
      } else {
        return parent_ == null ?
            net.sf.okapi.proto.events.TextContainer.getDefaultInstance() : parent_;
      }
    }
    /**
     * <code>optional .TextContainer parent = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilder<
        net.sf.okapi.proto.events.TextContainer, net.sf.okapi.proto.events.TextContainer.Builder, net.sf.okapi.proto.events.TextContainerOrBuilder> 
        getParentFieldBuilder() {
      if (parentBuilder_ == null) {
        parentBuilder_ = new com.google.protobuf.SingleFieldBuilder<
            net.sf.okapi.proto.events.TextContainer, net.sf.okapi.proto.events.TextContainer.Builder, net.sf.okapi.proto.events.TextContainerOrBuilder>(
                getParent(),
                getParentForChildren(),
                isClean());
        parent_ = null;
      }
      return parentBuilder_;
    }

    private java.util.List<net.sf.okapi.proto.events.TextPart> parts_ =
      java.util.Collections.emptyList();
    private void ensurePartsIsMutable() {
      if (!((bitField0_ & 0x00000004) == 0x00000004)) {
        parts_ = new java.util.ArrayList<net.sf.okapi.proto.events.TextPart>(parts_);
        bitField0_ |= 0x00000004;
       }
    }

    private com.google.protobuf.RepeatedFieldBuilder<
        net.sf.okapi.proto.events.TextPart, net.sf.okapi.proto.events.TextPart.Builder, net.sf.okapi.proto.events.TextPartOrBuilder> partsBuilder_;

    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public java.util.List<net.sf.okapi.proto.events.TextPart> getPartsList() {
      if (partsBuilder_ == null) {
        return java.util.Collections.unmodifiableList(parts_);
      } else {
        return partsBuilder_.getMessageList();
      }
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public int getPartsCount() {
      if (partsBuilder_ == null) {
        return parts_.size();
      } else {
        return partsBuilder_.getCount();
      }
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public net.sf.okapi.proto.events.TextPart getParts(int index) {
      if (partsBuilder_ == null) {
        return parts_.get(index);
      } else {
        return partsBuilder_.getMessage(index);
      }
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public Builder setParts(
        int index, net.sf.okapi.proto.events.TextPart value) {
      if (partsBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensurePartsIsMutable();
        parts_.set(index, value);
        onChanged();
      } else {
        partsBuilder_.setMessage(index, value);
      }
      return this;
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public Builder setParts(
        int index, net.sf.okapi.proto.events.TextPart.Builder builderForValue) {
      if (partsBuilder_ == null) {
        ensurePartsIsMutable();
        parts_.set(index, builderForValue.build());
        onChanged();
      } else {
        partsBuilder_.setMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public Builder addParts(net.sf.okapi.proto.events.TextPart value) {
      if (partsBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensurePartsIsMutable();
        parts_.add(value);
        onChanged();
      } else {
        partsBuilder_.addMessage(value);
      }
      return this;
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public Builder addParts(
        int index, net.sf.okapi.proto.events.TextPart value) {
      if (partsBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensurePartsIsMutable();
        parts_.add(index, value);
        onChanged();
      } else {
        partsBuilder_.addMessage(index, value);
      }
      return this;
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public Builder addParts(
        net.sf.okapi.proto.events.TextPart.Builder builderForValue) {
      if (partsBuilder_ == null) {
        ensurePartsIsMutable();
        parts_.add(builderForValue.build());
        onChanged();
      } else {
        partsBuilder_.addMessage(builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public Builder addParts(
        int index, net.sf.okapi.proto.events.TextPart.Builder builderForValue) {
      if (partsBuilder_ == null) {
        ensurePartsIsMutable();
        parts_.add(index, builderForValue.build());
        onChanged();
      } else {
        partsBuilder_.addMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public Builder addAllParts(
        java.lang.Iterable<? extends net.sf.okapi.proto.events.TextPart> values) {
      if (partsBuilder_ == null) {
        ensurePartsIsMutable();
        com.google.protobuf.AbstractMessageLite.Builder.addAll(
            values, parts_);
        onChanged();
      } else {
        partsBuilder_.addAllMessages(values);
      }
      return this;
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public Builder clearParts() {
      if (partsBuilder_ == null) {
        parts_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000004);
        onChanged();
      } else {
        partsBuilder_.clear();
      }
      return this;
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public Builder removeParts(int index) {
      if (partsBuilder_ == null) {
        ensurePartsIsMutable();
        parts_.remove(index);
        onChanged();
      } else {
        partsBuilder_.remove(index);
      }
      return this;
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public net.sf.okapi.proto.events.TextPart.Builder getPartsBuilder(
        int index) {
      return getPartsFieldBuilder().getBuilder(index);
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public net.sf.okapi.proto.events.TextPartOrBuilder getPartsOrBuilder(
        int index) {
      if (partsBuilder_ == null) {
        return parts_.get(index);  } else {
        return partsBuilder_.getMessageOrBuilder(index);
      }
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public java.util.List<? extends net.sf.okapi.proto.events.TextPartOrBuilder> 
         getPartsOrBuilderList() {
      if (partsBuilder_ != null) {
        return partsBuilder_.getMessageOrBuilderList();
      } else {
        return java.util.Collections.unmodifiableList(parts_);
      }
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public net.sf.okapi.proto.events.TextPart.Builder addPartsBuilder() {
      return getPartsFieldBuilder().addBuilder(
          net.sf.okapi.proto.events.TextPart.getDefaultInstance());
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public net.sf.okapi.proto.events.TextPart.Builder addPartsBuilder(
        int index) {
      return getPartsFieldBuilder().addBuilder(
          index, net.sf.okapi.proto.events.TextPart.getDefaultInstance());
    }
    /**
     * <code>repeated .TextPart parts = 3;</code>
     */
    public java.util.List<net.sf.okapi.proto.events.TextPart.Builder> 
         getPartsBuilderList() {
      return getPartsFieldBuilder().getBuilderList();
    }
    private com.google.protobuf.RepeatedFieldBuilder<
        net.sf.okapi.proto.events.TextPart, net.sf.okapi.proto.events.TextPart.Builder, net.sf.okapi.proto.events.TextPartOrBuilder> 
        getPartsFieldBuilder() {
      if (partsBuilder_ == null) {
        partsBuilder_ = new com.google.protobuf.RepeatedFieldBuilder<
            net.sf.okapi.proto.events.TextPart, net.sf.okapi.proto.events.TextPart.Builder, net.sf.okapi.proto.events.TextPartOrBuilder>(
                parts_,
                ((bitField0_ & 0x00000004) == 0x00000004),
                getParentForChildren(),
                isClean());
        parts_ = null;
      }
      return partsBuilder_;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:Segments)
  }

  // @@protoc_insertion_point(class_scope:Segments)
  private static final net.sf.okapi.proto.events.Segments DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new net.sf.okapi.proto.events.Segments();
  }

  public static net.sf.okapi.proto.events.Segments getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<Segments>
      PARSER = new com.google.protobuf.AbstractParser<Segments>() {
    public Segments parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new Segments(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<Segments> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<Segments> getParserForType() {
    return PARSER;
  }

  public net.sf.okapi.proto.events.Segments getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

