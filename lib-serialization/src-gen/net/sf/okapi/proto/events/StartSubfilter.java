// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: main/proto/TextUnit.proto

package net.sf.okapi.proto.events;

/**
 * <pre>
 * StartSubfilter extends StartGroup
 * </pre>
 *
 * Protobuf type {@code StartSubfilter}
 */
public  final class StartSubfilter extends
    com.google.protobuf.GeneratedMessage implements
    // @@protoc_insertion_point(message_implements:StartSubfilter)
    StartSubfilterOrBuilder {
  // Use StartSubfilter.newBuilder() to construct.
  private StartSubfilter(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
    super(builder);
  }
  private StartSubfilter() {
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private StartSubfilter(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 10: {
            net.sf.okapi.proto.events.INameable.Builder subBuilder = null;
            if (nameable_ != null) {
              subBuilder = nameable_.toBuilder();
            }
            nameable_ = input.readMessage(net.sf.okapi.proto.events.INameable.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(nameable_);
              nameable_ = subBuilder.buildPartial();
            }

            break;
          }
          case 18: {
            net.sf.okapi.proto.events.StartDocument.Builder subBuilder = null;
            if (startDoc_ != null) {
              subBuilder = startDoc_.toBuilder();
            }
            startDoc_ = input.readMessage(net.sf.okapi.proto.events.StartDocument.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(startDoc_);
              startDoc_ = subBuilder.buildPartial();
            }

            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_StartSubfilter_descriptor;
  }

  protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_StartSubfilter_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            net.sf.okapi.proto.events.StartSubfilter.class, net.sf.okapi.proto.events.StartSubfilter.Builder.class);
  }

  public static final int NAMEABLE_FIELD_NUMBER = 1;
  private net.sf.okapi.proto.events.INameable nameable_;
  /**
   * <code>optional .INameable nameable = 1;</code>
   */
  public boolean hasNameable() {
    return nameable_ != null;
  }
  /**
   * <code>optional .INameable nameable = 1;</code>
   */
  public net.sf.okapi.proto.events.INameable getNameable() {
    return nameable_ == null ? net.sf.okapi.proto.events.INameable.getDefaultInstance() : nameable_;
  }
  /**
   * <code>optional .INameable nameable = 1;</code>
   */
  public net.sf.okapi.proto.events.INameableOrBuilder getNameableOrBuilder() {
    return getNameable();
  }

  public static final int STARTDOC_FIELD_NUMBER = 2;
  private net.sf.okapi.proto.events.StartDocument startDoc_;
  /**
   * <pre>
   * SubFilterSkeletonWriter skelWriter;
   * IEncoder parentEncoder;
   * </pre>
   *
   * <code>optional .StartDocument startDoc = 2;</code>
   */
  public boolean hasStartDoc() {
    return startDoc_ != null;
  }
  /**
   * <pre>
   * SubFilterSkeletonWriter skelWriter;
   * IEncoder parentEncoder;
   * </pre>
   *
   * <code>optional .StartDocument startDoc = 2;</code>
   */
  public net.sf.okapi.proto.events.StartDocument getStartDoc() {
    return startDoc_ == null ? net.sf.okapi.proto.events.StartDocument.getDefaultInstance() : startDoc_;
  }
  /**
   * <pre>
   * SubFilterSkeletonWriter skelWriter;
   * IEncoder parentEncoder;
   * </pre>
   *
   * <code>optional .StartDocument startDoc = 2;</code>
   */
  public net.sf.okapi.proto.events.StartDocumentOrBuilder getStartDocOrBuilder() {
    return getStartDoc();
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (nameable_ != null) {
      output.writeMessage(1, getNameable());
    }
    if (startDoc_ != null) {
      output.writeMessage(2, getStartDoc());
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (nameable_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getNameable());
    }
    if (startDoc_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getStartDoc());
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  public static net.sf.okapi.proto.events.StartSubfilter parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static net.sf.okapi.proto.events.StartSubfilter parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.StartSubfilter parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static net.sf.okapi.proto.events.StartSubfilter parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.StartSubfilter parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static net.sf.okapi.proto.events.StartSubfilter parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.StartSubfilter parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static net.sf.okapi.proto.events.StartSubfilter parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.StartSubfilter parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static net.sf.okapi.proto.events.StartSubfilter parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(net.sf.okapi.proto.events.StartSubfilter prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessage.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * StartSubfilter extends StartGroup
   * </pre>
   *
   * Protobuf type {@code StartSubfilter}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessage.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:StartSubfilter)
      net.sf.okapi.proto.events.StartSubfilterOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_StartSubfilter_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_StartSubfilter_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              net.sf.okapi.proto.events.StartSubfilter.class, net.sf.okapi.proto.events.StartSubfilter.Builder.class);
    }

    // Construct using net.sf.okapi.proto.events.StartSubfilter.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      if (nameableBuilder_ == null) {
        nameable_ = null;
      } else {
        nameable_ = null;
        nameableBuilder_ = null;
      }
      if (startDocBuilder_ == null) {
        startDoc_ = null;
      } else {
        startDoc_ = null;
        startDocBuilder_ = null;
      }
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_StartSubfilter_descriptor;
    }

    public net.sf.okapi.proto.events.StartSubfilter getDefaultInstanceForType() {
      return net.sf.okapi.proto.events.StartSubfilter.getDefaultInstance();
    }

    public net.sf.okapi.proto.events.StartSubfilter build() {
      net.sf.okapi.proto.events.StartSubfilter result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public net.sf.okapi.proto.events.StartSubfilter buildPartial() {
      net.sf.okapi.proto.events.StartSubfilter result = new net.sf.okapi.proto.events.StartSubfilter(this);
      if (nameableBuilder_ == null) {
        result.nameable_ = nameable_;
      } else {
        result.nameable_ = nameableBuilder_.build();
      }
      if (startDocBuilder_ == null) {
        result.startDoc_ = startDoc_;
      } else {
        result.startDoc_ = startDocBuilder_.build();
      }
      onBuilt();
      return result;
    }

    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof net.sf.okapi.proto.events.StartSubfilter) {
        return mergeFrom((net.sf.okapi.proto.events.StartSubfilter)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(net.sf.okapi.proto.events.StartSubfilter other) {
      if (other == net.sf.okapi.proto.events.StartSubfilter.getDefaultInstance()) return this;
      if (other.hasNameable()) {
        mergeNameable(other.getNameable());
      }
      if (other.hasStartDoc()) {
        mergeStartDoc(other.getStartDoc());
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      net.sf.okapi.proto.events.StartSubfilter parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (net.sf.okapi.proto.events.StartSubfilter) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private net.sf.okapi.proto.events.INameable nameable_ = null;
    private com.google.protobuf.SingleFieldBuilder<
        net.sf.okapi.proto.events.INameable, net.sf.okapi.proto.events.INameable.Builder, net.sf.okapi.proto.events.INameableOrBuilder> nameableBuilder_;
    /**
     * <code>optional .INameable nameable = 1;</code>
     */
    public boolean hasNameable() {
      return nameableBuilder_ != null || nameable_ != null;
    }
    /**
     * <code>optional .INameable nameable = 1;</code>
     */
    public net.sf.okapi.proto.events.INameable getNameable() {
      if (nameableBuilder_ == null) {
        return nameable_ == null ? net.sf.okapi.proto.events.INameable.getDefaultInstance() : nameable_;
      } else {
        return nameableBuilder_.getMessage();
      }
    }
    /**
     * <code>optional .INameable nameable = 1;</code>
     */
    public Builder setNameable(net.sf.okapi.proto.events.INameable value) {
      if (nameableBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        nameable_ = value;
        onChanged();
      } else {
        nameableBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>optional .INameable nameable = 1;</code>
     */
    public Builder setNameable(
        net.sf.okapi.proto.events.INameable.Builder builderForValue) {
      if (nameableBuilder_ == null) {
        nameable_ = builderForValue.build();
        onChanged();
      } else {
        nameableBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>optional .INameable nameable = 1;</code>
     */
    public Builder mergeNameable(net.sf.okapi.proto.events.INameable value) {
      if (nameableBuilder_ == null) {
        if (nameable_ != null) {
          nameable_ =
            net.sf.okapi.proto.events.INameable.newBuilder(nameable_).mergeFrom(value).buildPartial();
        } else {
          nameable_ = value;
        }
        onChanged();
      } else {
        nameableBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>optional .INameable nameable = 1;</code>
     */
    public Builder clearNameable() {
      if (nameableBuilder_ == null) {
        nameable_ = null;
        onChanged();
      } else {
        nameable_ = null;
        nameableBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>optional .INameable nameable = 1;</code>
     */
    public net.sf.okapi.proto.events.INameable.Builder getNameableBuilder() {
      
      onChanged();
      return getNameableFieldBuilder().getBuilder();
    }
    /**
     * <code>optional .INameable nameable = 1;</code>
     */
    public net.sf.okapi.proto.events.INameableOrBuilder getNameableOrBuilder() {
      if (nameableBuilder_ != null) {
        return nameableBuilder_.getMessageOrBuilder();
      } else {
        return nameable_ == null ?
            net.sf.okapi.proto.events.INameable.getDefaultInstance() : nameable_;
      }
    }
    /**
     * <code>optional .INameable nameable = 1;</code>
     */
    private com.google.protobuf.SingleFieldBuilder<
        net.sf.okapi.proto.events.INameable, net.sf.okapi.proto.events.INameable.Builder, net.sf.okapi.proto.events.INameableOrBuilder> 
        getNameableFieldBuilder() {
      if (nameableBuilder_ == null) {
        nameableBuilder_ = new com.google.protobuf.SingleFieldBuilder<
            net.sf.okapi.proto.events.INameable, net.sf.okapi.proto.events.INameable.Builder, net.sf.okapi.proto.events.INameableOrBuilder>(
                getNameable(),
                getParentForChildren(),
                isClean());
        nameable_ = null;
      }
      return nameableBuilder_;
    }

    private net.sf.okapi.proto.events.StartDocument startDoc_ = null;
    private com.google.protobuf.SingleFieldBuilder<
        net.sf.okapi.proto.events.StartDocument, net.sf.okapi.proto.events.StartDocument.Builder, net.sf.okapi.proto.events.StartDocumentOrBuilder> startDocBuilder_;
    /**
     * <pre>
     * SubFilterSkeletonWriter skelWriter;
     * IEncoder parentEncoder;
     * </pre>
     *
     * <code>optional .StartDocument startDoc = 2;</code>
     */
    public boolean hasStartDoc() {
      return startDocBuilder_ != null || startDoc_ != null;
    }
    /**
     * <pre>
     * SubFilterSkeletonWriter skelWriter;
     * IEncoder parentEncoder;
     * </pre>
     *
     * <code>optional .StartDocument startDoc = 2;</code>
     */
    public net.sf.okapi.proto.events.StartDocument getStartDoc() {
      if (startDocBuilder_ == null) {
        return startDoc_ == null ? net.sf.okapi.proto.events.StartDocument.getDefaultInstance() : startDoc_;
      } else {
        return startDocBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * SubFilterSkeletonWriter skelWriter;
     * IEncoder parentEncoder;
     * </pre>
     *
     * <code>optional .StartDocument startDoc = 2;</code>
     */
    public Builder setStartDoc(net.sf.okapi.proto.events.StartDocument value) {
      if (startDocBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        startDoc_ = value;
        onChanged();
      } else {
        startDocBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <pre>
     * SubFilterSkeletonWriter skelWriter;
     * IEncoder parentEncoder;
     * </pre>
     *
     * <code>optional .StartDocument startDoc = 2;</code>
     */
    public Builder setStartDoc(
        net.sf.okapi.proto.events.StartDocument.Builder builderForValue) {
      if (startDocBuilder_ == null) {
        startDoc_ = builderForValue.build();
        onChanged();
      } else {
        startDocBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <pre>
     * SubFilterSkeletonWriter skelWriter;
     * IEncoder parentEncoder;
     * </pre>
     *
     * <code>optional .StartDocument startDoc = 2;</code>
     */
    public Builder mergeStartDoc(net.sf.okapi.proto.events.StartDocument value) {
      if (startDocBuilder_ == null) {
        if (startDoc_ != null) {
          startDoc_ =
            net.sf.okapi.proto.events.StartDocument.newBuilder(startDoc_).mergeFrom(value).buildPartial();
        } else {
          startDoc_ = value;
        }
        onChanged();
      } else {
        startDocBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <pre>
     * SubFilterSkeletonWriter skelWriter;
     * IEncoder parentEncoder;
     * </pre>
     *
     * <code>optional .StartDocument startDoc = 2;</code>
     */
    public Builder clearStartDoc() {
      if (startDocBuilder_ == null) {
        startDoc_ = null;
        onChanged();
      } else {
        startDoc_ = null;
        startDocBuilder_ = null;
      }

      return this;
    }
    /**
     * <pre>
     * SubFilterSkeletonWriter skelWriter;
     * IEncoder parentEncoder;
     * </pre>
     *
     * <code>optional .StartDocument startDoc = 2;</code>
     */
    public net.sf.okapi.proto.events.StartDocument.Builder getStartDocBuilder() {
      
      onChanged();
      return getStartDocFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * SubFilterSkeletonWriter skelWriter;
     * IEncoder parentEncoder;
     * </pre>
     *
     * <code>optional .StartDocument startDoc = 2;</code>
     */
    public net.sf.okapi.proto.events.StartDocumentOrBuilder getStartDocOrBuilder() {
      if (startDocBuilder_ != null) {
        return startDocBuilder_.getMessageOrBuilder();
      } else {
        return startDoc_ == null ?
            net.sf.okapi.proto.events.StartDocument.getDefaultInstance() : startDoc_;
      }
    }
    /**
     * <pre>
     * SubFilterSkeletonWriter skelWriter;
     * IEncoder parentEncoder;
     * </pre>
     *
     * <code>optional .StartDocument startDoc = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilder<
        net.sf.okapi.proto.events.StartDocument, net.sf.okapi.proto.events.StartDocument.Builder, net.sf.okapi.proto.events.StartDocumentOrBuilder> 
        getStartDocFieldBuilder() {
      if (startDocBuilder_ == null) {
        startDocBuilder_ = new com.google.protobuf.SingleFieldBuilder<
            net.sf.okapi.proto.events.StartDocument, net.sf.okapi.proto.events.StartDocument.Builder, net.sf.okapi.proto.events.StartDocumentOrBuilder>(
                getStartDoc(),
                getParentForChildren(),
                isClean());
        startDoc_ = null;
      }
      return startDocBuilder_;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:StartSubfilter)
  }

  // @@protoc_insertion_point(class_scope:StartSubfilter)
  private static final net.sf.okapi.proto.events.StartSubfilter DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new net.sf.okapi.proto.events.StartSubfilter();
  }

  public static net.sf.okapi.proto.events.StartSubfilter getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<StartSubfilter>
      PARSER = new com.google.protobuf.AbstractParser<StartSubfilter>() {
    public StartSubfilter parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new StartSubfilter(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<StartSubfilter> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<StartSubfilter> getParserForType() {
    return PARSER;
  }

  public net.sf.okapi.proto.events.StartSubfilter getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

