// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: main/proto/TextUnit.proto

package net.sf.okapi.proto.events;

/**
 * <pre>
 * StartGroup extends BaseReferenceable
 * BaseReferenceable extends BaseNameable implements IReferenceable
 * BaseNameable implements INameable
 * </pre>
 *
 * Protobuf type {@code StartGroup}
 */
public  final class StartGroup extends
    com.google.protobuf.GeneratedMessage implements
    // @@protoc_insertion_point(message_implements:StartGroup)
    StartGroupOrBuilder {
  // Use StartGroup.newBuilder() to construct.
  private StartGroup(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
    super(builder);
  }
  private StartGroup() {
    parentId_ = "";
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private StartGroup(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 18: {
            java.lang.String s = input.readStringRequireUtf8();

            parentId_ = s;
            break;
          }
          case 26: {
            net.sf.okapi.proto.events.INameable.Builder subBuilder = null;
            if (nameable_ != null) {
              subBuilder = nameable_.toBuilder();
            }
            nameable_ = input.readMessage(net.sf.okapi.proto.events.INameable.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(nameable_);
              nameable_ = subBuilder.buildPartial();
            }

            break;
          }
          case 34: {
            net.sf.okapi.proto.events.IReferenceable.Builder subBuilder = null;
            if (referenceable_ != null) {
              subBuilder = referenceable_.toBuilder();
            }
            referenceable_ = input.readMessage(net.sf.okapi.proto.events.IReferenceable.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(referenceable_);
              referenceable_ = subBuilder.buildPartial();
            }

            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_StartGroup_descriptor;
  }

  protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_StartGroup_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            net.sf.okapi.proto.events.StartGroup.class, net.sf.okapi.proto.events.StartGroup.Builder.class);
  }

  public static final int PARENTID_FIELD_NUMBER = 2;
  private volatile java.lang.Object parentId_;
  /**
   * <pre>
   * from BaseReferenceable
   * </pre>
   *
   * <code>optional string parentId = 2;</code>
   */
  public java.lang.String getParentId() {
    java.lang.Object ref = parentId_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      parentId_ = s;
      return s;
    }
  }
  /**
   * <pre>
   * from BaseReferenceable
   * </pre>
   *
   * <code>optional string parentId = 2;</code>
   */
  public com.google.protobuf.ByteString
      getParentIdBytes() {
    java.lang.Object ref = parentId_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      parentId_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int NAMEABLE_FIELD_NUMBER = 3;
  private net.sf.okapi.proto.events.INameable nameable_;
  /**
   * <code>optional .INameable nameable = 3;</code>
   */
  public boolean hasNameable() {
    return nameable_ != null;
  }
  /**
   * <code>optional .INameable nameable = 3;</code>
   */
  public net.sf.okapi.proto.events.INameable getNameable() {
    return nameable_ == null ? net.sf.okapi.proto.events.INameable.getDefaultInstance() : nameable_;
  }
  /**
   * <code>optional .INameable nameable = 3;</code>
   */
  public net.sf.okapi.proto.events.INameableOrBuilder getNameableOrBuilder() {
    return getNameable();
  }

  public static final int REFERENCEABLE_FIELD_NUMBER = 4;
  private net.sf.okapi.proto.events.IReferenceable referenceable_;
  /**
   * <code>optional .IReferenceable referenceable = 4;</code>
   */
  public boolean hasReferenceable() {
    return referenceable_ != null;
  }
  /**
   * <code>optional .IReferenceable referenceable = 4;</code>
   */
  public net.sf.okapi.proto.events.IReferenceable getReferenceable() {
    return referenceable_ == null ? net.sf.okapi.proto.events.IReferenceable.getDefaultInstance() : referenceable_;
  }
  /**
   * <code>optional .IReferenceable referenceable = 4;</code>
   */
  public net.sf.okapi.proto.events.IReferenceableOrBuilder getReferenceableOrBuilder() {
    return getReferenceable();
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (!getParentIdBytes().isEmpty()) {
      com.google.protobuf.GeneratedMessage.writeString(output, 2, parentId_);
    }
    if (nameable_ != null) {
      output.writeMessage(3, getNameable());
    }
    if (referenceable_ != null) {
      output.writeMessage(4, getReferenceable());
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (!getParentIdBytes().isEmpty()) {
      size += com.google.protobuf.GeneratedMessage.computeStringSize(2, parentId_);
    }
    if (nameable_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(3, getNameable());
    }
    if (referenceable_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(4, getReferenceable());
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  public static net.sf.okapi.proto.events.StartGroup parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static net.sf.okapi.proto.events.StartGroup parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.StartGroup parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static net.sf.okapi.proto.events.StartGroup parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.StartGroup parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static net.sf.okapi.proto.events.StartGroup parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.StartGroup parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static net.sf.okapi.proto.events.StartGroup parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static net.sf.okapi.proto.events.StartGroup parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static net.sf.okapi.proto.events.StartGroup parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(net.sf.okapi.proto.events.StartGroup prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessage.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * StartGroup extends BaseReferenceable
   * BaseReferenceable extends BaseNameable implements IReferenceable
   * BaseNameable implements INameable
   * </pre>
   *
   * Protobuf type {@code StartGroup}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessage.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:StartGroup)
      net.sf.okapi.proto.events.StartGroupOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_StartGroup_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_StartGroup_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              net.sf.okapi.proto.events.StartGroup.class, net.sf.okapi.proto.events.StartGroup.Builder.class);
    }

    // Construct using net.sf.okapi.proto.events.StartGroup.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      parentId_ = "";

      if (nameableBuilder_ == null) {
        nameable_ = null;
      } else {
        nameable_ = null;
        nameableBuilder_ = null;
      }
      if (referenceableBuilder_ == null) {
        referenceable_ = null;
      } else {
        referenceable_ = null;
        referenceableBuilder_ = null;
      }
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return net.sf.okapi.proto.events.OkapiEventProtoMapping.internal_static_StartGroup_descriptor;
    }

    public net.sf.okapi.proto.events.StartGroup getDefaultInstanceForType() {
      return net.sf.okapi.proto.events.StartGroup.getDefaultInstance();
    }

    public net.sf.okapi.proto.events.StartGroup build() {
      net.sf.okapi.proto.events.StartGroup result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public net.sf.okapi.proto.events.StartGroup buildPartial() {
      net.sf.okapi.proto.events.StartGroup result = new net.sf.okapi.proto.events.StartGroup(this);
      result.parentId_ = parentId_;
      if (nameableBuilder_ == null) {
        result.nameable_ = nameable_;
      } else {
        result.nameable_ = nameableBuilder_.build();
      }
      if (referenceableBuilder_ == null) {
        result.referenceable_ = referenceable_;
      } else {
        result.referenceable_ = referenceableBuilder_.build();
      }
      onBuilt();
      return result;
    }

    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof net.sf.okapi.proto.events.StartGroup) {
        return mergeFrom((net.sf.okapi.proto.events.StartGroup)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(net.sf.okapi.proto.events.StartGroup other) {
      if (other == net.sf.okapi.proto.events.StartGroup.getDefaultInstance()) return this;
      if (!other.getParentId().isEmpty()) {
        parentId_ = other.parentId_;
        onChanged();
      }
      if (other.hasNameable()) {
        mergeNameable(other.getNameable());
      }
      if (other.hasReferenceable()) {
        mergeReferenceable(other.getReferenceable());
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      net.sf.okapi.proto.events.StartGroup parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (net.sf.okapi.proto.events.StartGroup) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private java.lang.Object parentId_ = "";
    /**
     * <pre>
     * from BaseReferenceable
     * </pre>
     *
     * <code>optional string parentId = 2;</code>
     */
    public java.lang.String getParentId() {
      java.lang.Object ref = parentId_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        parentId_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <pre>
     * from BaseReferenceable
     * </pre>
     *
     * <code>optional string parentId = 2;</code>
     */
    public com.google.protobuf.ByteString
        getParentIdBytes() {
      java.lang.Object ref = parentId_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        parentId_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <pre>
     * from BaseReferenceable
     * </pre>
     *
     * <code>optional string parentId = 2;</code>
     */
    public Builder setParentId(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      parentId_ = value;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * from BaseReferenceable
     * </pre>
     *
     * <code>optional string parentId = 2;</code>
     */
    public Builder clearParentId() {
      
      parentId_ = getDefaultInstance().getParentId();
      onChanged();
      return this;
    }
    /**
     * <pre>
     * from BaseReferenceable
     * </pre>
     *
     * <code>optional string parentId = 2;</code>
     */
    public Builder setParentIdBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      parentId_ = value;
      onChanged();
      return this;
    }

    private net.sf.okapi.proto.events.INameable nameable_ = null;
    private com.google.protobuf.SingleFieldBuilder<
        net.sf.okapi.proto.events.INameable, net.sf.okapi.proto.events.INameable.Builder, net.sf.okapi.proto.events.INameableOrBuilder> nameableBuilder_;
    /**
     * <code>optional .INameable nameable = 3;</code>
     */
    public boolean hasNameable() {
      return nameableBuilder_ != null || nameable_ != null;
    }
    /**
     * <code>optional .INameable nameable = 3;</code>
     */
    public net.sf.okapi.proto.events.INameable getNameable() {
      if (nameableBuilder_ == null) {
        return nameable_ == null ? net.sf.okapi.proto.events.INameable.getDefaultInstance() : nameable_;
      } else {
        return nameableBuilder_.getMessage();
      }
    }
    /**
     * <code>optional .INameable nameable = 3;</code>
     */
    public Builder setNameable(net.sf.okapi.proto.events.INameable value) {
      if (nameableBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        nameable_ = value;
        onChanged();
      } else {
        nameableBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>optional .INameable nameable = 3;</code>
     */
    public Builder setNameable(
        net.sf.okapi.proto.events.INameable.Builder builderForValue) {
      if (nameableBuilder_ == null) {
        nameable_ = builderForValue.build();
        onChanged();
      } else {
        nameableBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>optional .INameable nameable = 3;</code>
     */
    public Builder mergeNameable(net.sf.okapi.proto.events.INameable value) {
      if (nameableBuilder_ == null) {
        if (nameable_ != null) {
          nameable_ =
            net.sf.okapi.proto.events.INameable.newBuilder(nameable_).mergeFrom(value).buildPartial();
        } else {
          nameable_ = value;
        }
        onChanged();
      } else {
        nameableBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>optional .INameable nameable = 3;</code>
     */
    public Builder clearNameable() {
      if (nameableBuilder_ == null) {
        nameable_ = null;
        onChanged();
      } else {
        nameable_ = null;
        nameableBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>optional .INameable nameable = 3;</code>
     */
    public net.sf.okapi.proto.events.INameable.Builder getNameableBuilder() {
      
      onChanged();
      return getNameableFieldBuilder().getBuilder();
    }
    /**
     * <code>optional .INameable nameable = 3;</code>
     */
    public net.sf.okapi.proto.events.INameableOrBuilder getNameableOrBuilder() {
      if (nameableBuilder_ != null) {
        return nameableBuilder_.getMessageOrBuilder();
      } else {
        return nameable_ == null ?
            net.sf.okapi.proto.events.INameable.getDefaultInstance() : nameable_;
      }
    }
    /**
     * <code>optional .INameable nameable = 3;</code>
     */
    private com.google.protobuf.SingleFieldBuilder<
        net.sf.okapi.proto.events.INameable, net.sf.okapi.proto.events.INameable.Builder, net.sf.okapi.proto.events.INameableOrBuilder> 
        getNameableFieldBuilder() {
      if (nameableBuilder_ == null) {
        nameableBuilder_ = new com.google.protobuf.SingleFieldBuilder<
            net.sf.okapi.proto.events.INameable, net.sf.okapi.proto.events.INameable.Builder, net.sf.okapi.proto.events.INameableOrBuilder>(
                getNameable(),
                getParentForChildren(),
                isClean());
        nameable_ = null;
      }
      return nameableBuilder_;
    }

    private net.sf.okapi.proto.events.IReferenceable referenceable_ = null;
    private com.google.protobuf.SingleFieldBuilder<
        net.sf.okapi.proto.events.IReferenceable, net.sf.okapi.proto.events.IReferenceable.Builder, net.sf.okapi.proto.events.IReferenceableOrBuilder> referenceableBuilder_;
    /**
     * <code>optional .IReferenceable referenceable = 4;</code>
     */
    public boolean hasReferenceable() {
      return referenceableBuilder_ != null || referenceable_ != null;
    }
    /**
     * <code>optional .IReferenceable referenceable = 4;</code>
     */
    public net.sf.okapi.proto.events.IReferenceable getReferenceable() {
      if (referenceableBuilder_ == null) {
        return referenceable_ == null ? net.sf.okapi.proto.events.IReferenceable.getDefaultInstance() : referenceable_;
      } else {
        return referenceableBuilder_.getMessage();
      }
    }
    /**
     * <code>optional .IReferenceable referenceable = 4;</code>
     */
    public Builder setReferenceable(net.sf.okapi.proto.events.IReferenceable value) {
      if (referenceableBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        referenceable_ = value;
        onChanged();
      } else {
        referenceableBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>optional .IReferenceable referenceable = 4;</code>
     */
    public Builder setReferenceable(
        net.sf.okapi.proto.events.IReferenceable.Builder builderForValue) {
      if (referenceableBuilder_ == null) {
        referenceable_ = builderForValue.build();
        onChanged();
      } else {
        referenceableBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>optional .IReferenceable referenceable = 4;</code>
     */
    public Builder mergeReferenceable(net.sf.okapi.proto.events.IReferenceable value) {
      if (referenceableBuilder_ == null) {
        if (referenceable_ != null) {
          referenceable_ =
            net.sf.okapi.proto.events.IReferenceable.newBuilder(referenceable_).mergeFrom(value).buildPartial();
        } else {
          referenceable_ = value;
        }
        onChanged();
      } else {
        referenceableBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>optional .IReferenceable referenceable = 4;</code>
     */
    public Builder clearReferenceable() {
      if (referenceableBuilder_ == null) {
        referenceable_ = null;
        onChanged();
      } else {
        referenceable_ = null;
        referenceableBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>optional .IReferenceable referenceable = 4;</code>
     */
    public net.sf.okapi.proto.events.IReferenceable.Builder getReferenceableBuilder() {
      
      onChanged();
      return getReferenceableFieldBuilder().getBuilder();
    }
    /**
     * <code>optional .IReferenceable referenceable = 4;</code>
     */
    public net.sf.okapi.proto.events.IReferenceableOrBuilder getReferenceableOrBuilder() {
      if (referenceableBuilder_ != null) {
        return referenceableBuilder_.getMessageOrBuilder();
      } else {
        return referenceable_ == null ?
            net.sf.okapi.proto.events.IReferenceable.getDefaultInstance() : referenceable_;
      }
    }
    /**
     * <code>optional .IReferenceable referenceable = 4;</code>
     */
    private com.google.protobuf.SingleFieldBuilder<
        net.sf.okapi.proto.events.IReferenceable, net.sf.okapi.proto.events.IReferenceable.Builder, net.sf.okapi.proto.events.IReferenceableOrBuilder> 
        getReferenceableFieldBuilder() {
      if (referenceableBuilder_ == null) {
        referenceableBuilder_ = new com.google.protobuf.SingleFieldBuilder<
            net.sf.okapi.proto.events.IReferenceable, net.sf.okapi.proto.events.IReferenceable.Builder, net.sf.okapi.proto.events.IReferenceableOrBuilder>(
                getReferenceable(),
                getParentForChildren(),
                isClean());
        referenceable_ = null;
      }
      return referenceableBuilder_;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:StartGroup)
  }

  // @@protoc_insertion_point(class_scope:StartGroup)
  private static final net.sf.okapi.proto.events.StartGroup DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new net.sf.okapi.proto.events.StartGroup();
  }

  public static net.sf.okapi.proto.events.StartGroup getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<StartGroup>
      PARSER = new com.google.protobuf.AbstractParser<StartGroup>() {
    public StartGroup parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new StartGroup(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<StartGroup> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<StartGroup> getParserForType() {
    return PARSER;
  }

  public net.sf.okapi.proto.events.StartGroup getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

