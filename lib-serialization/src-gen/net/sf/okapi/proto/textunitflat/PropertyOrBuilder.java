// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: main/proto/TextUnitFlat.proto

package net.sf.okapi.proto.textunitflat;

public interface PropertyOrBuilder extends
    // @@protoc_insertion_point(interface_extends:Property)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>optional string name = 1;</code>
   */
  java.lang.String getName();
  /**
   * <code>optional string name = 1;</code>
   */
  com.google.protobuf.ByteString
      getNameBytes();

  /**
   * <code>optional string value = 2;</code>
   */
  java.lang.String getValue();
  /**
   * <code>optional string value = 2;</code>
   */
  com.google.protobuf.ByteString
      getValueBytes();

  /**
   * <code>optional bool isReadOnly = 3;</code>
   */
  boolean getIsReadOnly();
}
