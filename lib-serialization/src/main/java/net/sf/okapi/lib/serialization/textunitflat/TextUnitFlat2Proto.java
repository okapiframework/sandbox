/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.serialization.textunitflat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.resource.TextPart;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.proto.textunitflat.TextContainer.Builder;

public class TextUnitFlat2Proto {

	static net.sf.okapi.proto.textunitflat.TextFragment.TagType toTagType(TagType tt) {
		switch (tt) {
		case CLOSING:
			return net.sf.okapi.proto.textunitflat.TextFragment.TagType.CLOSING;
		case OPENING:
			return net.sf.okapi.proto.textunitflat.TextFragment.TagType.OPENING;
		case PLACEHOLDER:
			return net.sf.okapi.proto.textunitflat.TextFragment.TagType.PLACEHOLDER;
		default:
			return net.sf.okapi.proto.textunitflat.TextFragment.TagType.UNRECOGNIZED;
		}
	}

	static net.sf.okapi.proto.textunitflat.Code toCode(Code code, int position, boolean isolated) {
		net.sf.okapi.proto.textunitflat.Code.Builder builder = net.sf.okapi.proto.textunitflat.Code.newBuilder()
				.setTagType(toTagType(code.getTagType())).setId(code.getId()).setCodeType(code.getType()).setData(code.getData())
				.setOuterData(code.getOuterData()).setHasOuterData(code.hasOuterData()).setFlag(code.getFlag());

		builder.setPosition(position);
		builder.setIsolated(isolated);
		builder.setAdded(code.isAdded());
		if (null != code.getOriginalId()) {
			builder.setOriginalId(code.getOriginalId());
		}
		
		if (null != code.getMergedData() && !code.getMergedData().isEmpty()) {
			builder.setMergedData(code.getMergedData());
		}
		return builder.build();
	}

	static net.sf.okapi.proto.textunitflat.TextFragment toTextFragment(String id, boolean isSegmented, TextFragment tf) {
		net.sf.okapi.proto.textunitflat.TextFragment.Builder builder = net.sf.okapi.proto.textunitflat.TextFragment.newBuilder()
				.setText(tf.getText());
		builder.setId(id);
		builder.setIsSegment(isSegmented);
		
		if (tf.hasCode()) {
			tf.balanceMarkers();	
			List<Code> codes = tf.getCodes(); 
			int codeCount = 0;
			for ( int i=0; i<tf.length(); i++ ) {
				switch (tf.charAt(i)) {
				case TextFragment.MARKER_OPENING:
				case TextFragment.MARKER_CLOSING:
				case TextFragment.MARKER_ISOLATED:
					Code code = codes.get(TextFragment.toIndex(tf.charAt(++i)));
					int newPos = i - (codeCount*2); 
					builder.addCodes(toCode(code, newPos-1, tf.charAt(i-1) == TextFragment.MARKER_ISOLATED ? true : false));
					codeCount++;
					break;
				default:
					break;
				}
			}
		}
		return builder.build();
	}
	
	static net.sf.okapi.proto.textunitflat.Property toProperty(Property prop) {
		net.sf.okapi.proto.textunitflat.Property.Builder builder = net.sf.okapi.proto.textunitflat.Property.newBuilder()
				.setName(prop.getName())
				.setValue(prop.getValue())
				.setIsReadOnly(prop.isReadOnly());

		return builder.build();
	}


	static net.sf.okapi.proto.textunitflat.TextContainer toTextContainer(TextContainer tc, LocaleId locale) {
		Builder builder = net.sf.okapi.proto.textunitflat.TextContainer.newBuilder().
				setSegApplied(tc.hasBeenSegmented()).setLocale(locale.toBCP47());
		for (TextPart part : tc.getParts()) {
			String id = part.isSegment() ? ((Segment)part).id : "";
			builder.addFragments(toTextFragment(id, part.isSegment(), part.getContent()));
		}
		
		for (String propName : tc.getPropertyNames()) {
			builder.addProperties(toProperty(tc.getProperty(propName)));
		}

		return builder.build();
	}

	public static net.sf.okapi.proto.textunitflat.TextUnit toTextUnit(ITextUnit tu, LocaleId sourceLocale) {
		net.sf.okapi.proto.textunitflat.TextUnit.Builder builder = net.sf.okapi.proto.textunitflat.TextUnit.newBuilder()				
				.setSource(toTextContainer(tu.getSource(), sourceLocale)).setId(tu.getId())
				.setPreserveWS(tu.preserveWhitespaces()).setTranslatable(tu.isTranslatable());

		if (null != tu.getMimeType()) {
			builder.setMimeType(tu.getMimeType());
		}
		
		if (null != tu.getName()) {
			builder.setName(tu.getName());
		}
		
		if (null != tu.getType()) {
			builder.setTuType(tu.getType());
		}
		
		Map<String, net.sf.okapi.proto.textunitflat.TextContainer> targets = new HashMap<>();
		for (LocaleId locale : tu.getTargetLocales()) {
			targets.put(locale.toBCP47(), toTextContainer(tu.getTarget(locale), locale));
		}
		builder.putAllTargets(targets);
		
		for (String propName : tu.getPropertyNames()) {
			builder.addProperties(toProperty(tu.getProperty(propName)));
		}

		return builder.build();
	}
	
	public static net.sf.okapi.proto.textunitflat.TextUnits toTextUnits(List<TextUnit> textunits, LocaleId sourceLocale) {
		net.sf.okapi.proto.textunitflat.TextUnits.Builder builder = net.sf.okapi.proto.textunitflat.TextUnits.newBuilder();
		for (TextUnit tu : textunits) {
			builder.addTextUnits(toTextUnit(tu, sourceLocale));
		}		
		return builder.build();
	}
}
