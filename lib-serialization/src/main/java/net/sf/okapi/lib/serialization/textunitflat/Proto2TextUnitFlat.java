/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.serialization.textunitflat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.resource.TextPart;
import net.sf.okapi.common.resource.TextUnit;

public class Proto2TextUnitFlat {

	static TagType toTagType(net.sf.okapi.proto.textunitflat.TextFragment.TagType tt) {
		switch (tt) {
			case CLOSING: return TagType.CLOSING;
			case OPENING: return TagType.OPENING;
			case PLACEHOLDER: return TagType.PLACEHOLDER;
			default: return TagType.valueOf("ERROR");
		}
	}

	static Code toCode(net.sf.okapi.proto.textunitflat.Code code) {
		Code result = new Code(toTagType(code.getTagType()), code.getCodeType(), code.getData());
		result.setId(code.getId());
		result.setOuterData(code.getOuterData());
		result.setFlag(code.getFlag());
		if (null != code.getOriginalId()) {
			result.setOriginalId(code.getOriginalId());
		}
		
		result.setAdded(code.getAdded());		
		
		if (null != code.getMergedData() && !code.getMergedData().isEmpty()) {
			result.setMergedData(code.getMergedData());
			result.setMerged(true);
		}
		
		return result;
	}

	static TextFragment toTextFragment(net.sf.okapi.proto.textunitflat.TextFragment tf) {
		TextFragment result = new TextFragment(tf.getText());
		int codeCount = 0;
		for (net.sf.okapi.proto.textunitflat.Code code : tf.getCodesList()) {			
			result.insert(code.getPosition() + (codeCount*2), toCode(code));
			codeCount++;
		}
		result.balanceMarkers();
		return result;
	}

	private static TextPart toTextPart(net.sf.okapi.proto.textunitflat.TextFragment fragment) {
		TextPart result = new TextPart(toTextFragment(fragment));
		if (fragment.getIsSegment()) {
			result = new Segment(fragment.getId(), toTextFragment(fragment));
		}
		return result;
	}
	
	static Property toProperty(net.sf.okapi.proto.textunitflat.Property prop) {
		Property result = new Property(prop.getName(), prop.getValue(), prop.getIsReadOnly());
		return result;
	}

	static TextContainer toTextContainer(net.sf.okapi.proto.textunitflat.TextContainer tc) {
		TextContainer result = new TextContainer();
		
		for (net.sf.okapi.proto.textunitflat.TextFragment fragment : tc.getFragmentsList()) {
			result.append(toTextPart(fragment));
		}
		result.setHasBeenSegmentedFlag(tc.getSegApplied());
		
		for (net.sf.okapi.proto.textunitflat.Property property : tc.getPropertiesList()) {
			result.setProperty(toProperty(property));	
		}
		
		return result;
	}

	public static TextUnit toTextUnit(net.sf.okapi.proto.textunitflat.TextUnit tu) {
		TextUnit result = new TextUnit(tu.getId(), "");
		
		result.setSource(toTextContainer(tu.getSource()));
		result.setMimeType(tu.getMimeType());
		result.setName(tu.getName());
		result.setType(tu.getTuType());
		result.setIsTranslatable(tu.getTranslatable());
		result.setPreserveWhitespaces(tu.getPreserveWS());

		for (Entry<String, net.sf.okapi.proto.textunitflat.TextContainer> trg : tu.getTargetsMap().entrySet()) {			
			result.setTarget(LocaleId.fromBCP47(trg.getKey()), toTextContainer(trg.getValue()));
		}

		for (net.sf.okapi.proto.textunitflat.Property property : tu.getPropertiesList()) {
			result.setProperty(toProperty(property));	
		}
		
		return result;
	}
	
	public static List<TextUnit> toTextUnits(net.sf.okapi.proto.textunitflat.TextUnits tus) {
		List<TextUnit> result = new ArrayList<TextUnit>();
		for (net.sf.okapi.proto.textunitflat.TextUnit tu : tus.getTextUnitsList()) {
			result.add(toTextUnit(tu));
		}
		return result;
	}
}
