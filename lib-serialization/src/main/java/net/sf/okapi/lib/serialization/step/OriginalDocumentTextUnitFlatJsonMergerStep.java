/*===========================================================================
  Copyright (C) 2009-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.serialization.step;

import java.io.OutputStream;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.exceptions.OkapiFilterCreationException;
import net.sf.okapi.common.exceptions.OkapiMergeException;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import net.sf.okapi.common.io.InputStreamFromOutputStream;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;
import net.sf.okapi.common.pipelinedriver.PipelineDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.lib.serialization.filter.TextUnitFlatJsonFilter;
import net.sf.okapi.lib.tkit.merge.SkeletonMergerWriter;
import net.sf.okapi.lib.tkit.merge.TextUnitMerger;

/**
 * Tkit merger which re-filters the original source file to provide the
 * skeleton for merging. Uses lib-tkit's {@link SkeletonMergerWriter} and {@link TextUnitMerger}.
 * 
 * @author jimh
 * 
 */
public class OriginalDocumentTextUnitFlatJsonMergerStep extends BasePipelineStep {
	private IFilter filter;
	private IFilterConfigurationMapper fcMapper;
	private String outputEncoding;
	private LocaleId trgLoc;
	private RawDocument originalDocument;
	
	public OriginalDocumentTextUnitFlatJsonMergerStep() {
	}
	
	@Override
	public String getName() {
		return "Original Document TextUnit Flat Json Merger";
	}

	@Override
	public String getDescription() {
		return "TextUnit Flat Json merger which re-filters the original source file to provide the skeleton for merging.";
	}

	@StepParameterMapping(parameterType = StepParameterType.OUTPUT_ENCODING)
	public void setOutputEncoding(String outputEncoding) {
		this.outputEncoding = outputEncoding;
	}
	
	/**
	 * Target locales. Currently only the first locale in the list is used.
	 * 
	 * @param targetLocales
	 */
	@StepParameterMapping(parameterType = StepParameterType.TARGET_LOCALES)
	public void setTargetLocales(final List<LocaleId> targetLocales) {
		this.trgLoc = targetLocales.get(0);
	}
	
	/**
	 * This is the original source document
	 * 
	 * @param secondInput Original source document
	 */
	@StepParameterMapping(parameterType = StepParameterType.SECOND_INPUT_RAWDOC)
	public void setSecondInput(final RawDocument secondInput) {
		this.originalDocument = secondInput;
	}

	/**
	 * The {@link IFilterConfigurationMapper} set in the {@link PipelineDriver}
	 * 
	 * @param fcMapper
	 */
	@StepParameterMapping(parameterType = StepParameterType.FILTER_CONFIGURATION_MAPPER)
	public void setFilterConfigurationMapper(final IFilterConfigurationMapper fcMapper) {
		this.fcMapper = fcMapper;
	}
	
	@SuppressWarnings("resource")
	@Override
	protected Event handleRawDocument(final Event event) {
		filter = fcMapper.createFilter(originalDocument.getFilterConfigId(), filter);
		if (filter == null) {
			throw new OkapiFilterCreationException(String.format(
					"Cannot create the filter or load the configuration for '%s'",
					originalDocument.getFilterConfigId()));
		}
		filter.open(originalDocument);
		final SkeletonMergerWriter skelMergerWriter = new SkeletonMergerWriter(filter, null);
		final TextUnitFlatJsonFilter jsonFilter = new TextUnitFlatJsonFilter();
		skelMergerWriter.setOptions(trgLoc, outputEncoding);
		skelMergerWriter.setParameters(getParameters());
		final InputStreamFromOutputStream<Void> is = new InputStreamFromOutputStream<Void>() {
			@Override
			protected Void produce(OutputStream sink) throws Exception {
				try {
					skelMergerWriter.setOutput(sink);
					jsonFilter.open(event.getRawDocument());
					while (jsonFilter.hasNext()) {
						skelMergerWriter.handleEvent(jsonFilter.next());
					}					
				} catch (Exception e) {
					close();
					throw new OkapiMergeException("Error merging from original file", e);
				} finally {
					if (jsonFilter != null) jsonFilter.close();
					skelMergerWriter.close();
					originalDocument.close();
				}
					
				return null;
			}
		};
						
		// Writer step closes the RawDocument
		return new Event(EventType.RAW_DOCUMENT, new RawDocument(is, outputEncoding, trgLoc));
	}
	
	@Override
	public void cancel() {
	}

	@Override
	public void destroy() {
	}
}
