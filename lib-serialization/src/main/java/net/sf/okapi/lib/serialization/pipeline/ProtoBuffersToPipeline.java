/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.serialization.pipeline;

import java.io.IOException;
import java.io.InputStream;

import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.pipeline.IPipeline;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.common.pipeline.Pipeline;

public final class ProtoBuffersToPipeline {

	static IPipeline toPipeline(net.sf.okapi.proto.pipeline.Pipeline protoPipeline)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		IPipeline okapiPipeline = new Pipeline();
		okapiPipeline.setId(protoPipeline.getId());
		for (net.sf.okapi.proto.pipeline.PipelineStep protoStep : protoPipeline.getStepsList()) {
			// FIXME what classloader should we use?
			IPipelineStep okapiStep = (IPipelineStep) Class
					.forName(protoStep.getStepClass(), true, Thread.currentThread().getContextClassLoader())
					.newInstance();
			IParameters p = okapiStep.getParameters();
			if (p != null) {
				p.fromString(protoStep.getParams());
				okapiStep.setParameters(p);
			}
			okapiStep.setLastOutputStep(protoStep.getIsLastOutputStep());
			okapiPipeline.addStep(okapiStep);
		}
		return okapiPipeline;
	}

	static IPipeline toPipeline(InputStream pipeline)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, IOException {
		return toPipeline(net.sf.okapi.proto.pipeline.Pipeline.parseFrom(pipeline));
	}
	
	static IPipeline toPipeline(byte[] pipeline)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, IOException {
		return toPipeline(net.sf.okapi.proto.pipeline.Pipeline.parseFrom(pipeline));
	}
}
